<?php require('chrome.php'); ct(); ?>

    <div class="subpage-header" data-bg="img/subheader.jpg"></div>

	<div class="row content-wrap">
		<div class="col-md-3 hidden-sm sidebar first-sidebar">
			<?php include("sidenews.php"); ?>
		</div> <!-- /end first sidebar -->

        <div class="col-md-13 col-sm-11 main-content">

    <h1>News and Press</h1>
<div id="widecentercontent">

  <h3>October</h3>

    <p class = "row">
      <span class = "leftcol">October 13, 2015</span>
      <span class = "rightcol"><a href="news.php">Chancellor's Forum: Kiss, Bow or Shake Hands, with Terri Morrison Oct. 26</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">October 08, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001483">Philadelphia Bar Association Announces Judicial Ratings, Releases Poll Results</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">October 05, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001480">Deadline Oct. 9: Honorable Louis H. Pollak Award</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">October 05, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001478">RESCHEDULED: Heather Jarvis Program</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">October 05, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001481">Deadline Oct. 9: Andrew Hamilton Award</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">October 05, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001479">Bar Academy: &quot;Disgraced&quot; at the Philadelphia Theatre Company Oct. 29</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>


  <h3>September</h3>

    <p class = "row">
      <span class = "leftcol">September 22, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001477">Notice to the Membership - Papal Visit</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">September 21, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001476">Climate Change and National Security Oct. 14</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">September 18, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001475">Philadelphia Bar Association Celebrates National Pro Bono Week</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">September 15, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001474">Giants of the Business Bar Oct. 22</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">September 15, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001473">Deadline Oct. 13: Nominations Sought for Brennan Award</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">September 15, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001472">Deadline Oct. 13: Nominations Sought for the PNC Achievement Award</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">September 08, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001471">&quot;Nuts &amp; Bolts&quot; of Running for Elected Positions Sept. 17</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">September 01, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001470">Free Legal Advice Offered Throughout September by Philadelphia Bar Association</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>


  <h3>August</h3>

    <p class = "row">
      <span class = "leftcol">August 31, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001468">Supreme Court Candidates Forum: Decision 2015 Sept. 18</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">August 31, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/WebObjects/PBA.woa/Contents/WebServerResources/CMSResources/IrvStanderAwardApplication2016.pdf">The 2016 Irvin Stander Award application period begins September 30, 2015 and ends January 29, 2016</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">August 24, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001467">&quot;Vatican Splendors&quot; VIP Guided Tour by Emily Urban, Ph.D. Sept. 29</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">August 24, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001466">2015 Bench-Bar &amp; Annual Conference Scholarship Opportunity</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>


  <h3>July</h3>

    <p class = "row">
      <span class = "leftcol">July 20, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001465">Message From the Chancellor: Clemency Project 2014 Seeks Volunteers</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">July 15, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001464">Public Speaking for Lawyers, Featuring John Savoth Aug. 6</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">July 13, 2015</span>
      <span class = "rightcol"><a href="http://goo.gl/4FXyek">Philadelphia Bar Association Seeks Senior Managing Editor of Publications</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">July 10, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001462">Summer-Long School Supply Drive</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">July 02, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001461">Gourmet Tasting, Demonstration at Gran Caffè L'Aquila July 22</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>


  <h3>June</h3>

    <p class = "row">
      <span class = "leftcol">June 26, 2015</span>
      <span class = "rightcol"><a href="https://youtu.be/X0l3VEdxNlc">Video: Philadelphia Bar Association Quarterly Meeting - June 9, 2015</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">June 26, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001460">Statement Regarding U.S. Supreme Court Ruling on Same-Sex Marriage</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">June 22, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001456">The Legal Intelligencer is Accepting Nominations for 2015 Lawyers on the Fast Track - Deadline June 26</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">June 22, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001458">2015 Annual Affinity Bar Association Quizzo Championship July 14</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">June 22, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001457">BLS/YLD Speed 180 Program July 8</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">June 18, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001455">Federal Bench-Bar Conference June 19</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">June 16, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001454">YLD Diversity Reception at Chima June 17 - Free Registration</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">June 09, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001453">Bar Foundation Golf/Tennis Classic June 15</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>


  <h3>May</h3>

    <p class = "row">
      <span class = "leftcol">May 20, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001452">Tickets on Sale Now for June 4 Philly Idol Talent Show </a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">May 07, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001451">Nominations Sought for Large Firm Management Committee Public Service Awards</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">May 05, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001450">Philadelphia Bar Association Releases Final Ratings of Judicial Candidates for May 19 Primary Election</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>


  <h3>April</h3>

    <p class = "row">
      <span class = "leftcol">April 30, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001449">Bar Offices Closing Early April 30</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">April 28, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001447">Pathway to a More Inclusive Bench CLE May 20</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">April 28, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001448">Effective Online Marketing Strategies May 20</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">April 24, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001446">Help Spread the Word About Recommended Candidates</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">April 24, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001445">Giants of Business Bar with Justin Klein May 14</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">April 23, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001444">Free Legal Advice to Mark Citywide Celebration of Law Week 2015</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">April 20, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001443">Take Bar Association Poll on Judges Seeking Retention in May Primary Through May 1</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">April 17, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001442">Ginsburg Essay Entry Deadline May 29</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">April 16, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001441">Judge Lewis to Deliver Higginbotham Lecture June 9; Carr to Accept O'Connor Award</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">April 08, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001439">Turning Points for Children to Honor Chancellor Dandridge April 23</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">April 07, 2015</span>
      <span class = "rightcol"><a href="https://pcntv.com/streaming-live-on-thursday-at-noon-pa-supreme-court-candidates-forum/">Decision 2015: Supreme Court Candidates Forum</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">April 02, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001438">Philadelphia Bar Association Announces Ratings of Appellate Court Candidates for May 19 Primary Election</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>


  <h3>March</h3>

    <p class = "row">
      <span class = "leftcol">March 30, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001435">Philadelphia Bar Association Releases Ratings of Judicial Candidates for May 19 Primary Election</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">March 30, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001437">&quot;Judgment in Berlin&quot; Author April 7</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">March 30, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001436">How Does Design Influence Profitability, Viability</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">March 20, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001434">36th Annual 5K Run/Walk May 17 </a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">March 11, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001433">Judge Nelson Diaz accepts the Justice Sonia Sotomayor Diversity Award</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">March 09, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001432">Serving Lunch for the Guests of St. John's Hospice on May 8 and July 10, 2015</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">March 06, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001431">Quarterly Meeting &amp; Luncheon Registration Deadline Today</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">March 05, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001430">Bar Offices Closed Mar.5 Due to Winter Storm</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">March 04, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001429">Oppose Sales Tax on Legal Services in PA</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">March 03, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001428">Bring a Canned or Other Food Item to Quarterly Luncheon to Benefit Philabundance</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">March 03, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001427">Bench-Bar &amp; Annual Conference Returns to Borgata Oct. 16-17</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>


  <h3>February</h3>

    <p class = "row">
      <span class = "leftcol">February 26, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001426">PBI Launches New Website</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">February 20, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001425">Supreme Court Candidates Forum March 19</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">February 17, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001424">Feb. 24 Charlie Hebdo, Free Speech Chancellor's Forum</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">February 12, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001423">Diaz to Receive Sotomayor Award at March 10 Quarterly Meeting</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">February 05, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001420">O'Connor Award Nomination Deadline is March 30</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">February 05, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001421">Law Firm Laboratory: Vetting Expert Witnesses Feb. 26</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">February 03, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001419">Justinians to Honor Dandridge at Feb. 24 Luncheon</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>


  <h3>January</h3>

    <p class = "row">
      <span class = "leftcol">January 29, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001418">Get Admitted to U.S. Supreme Court May 18</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">January 28, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001417">X, Y, Z's of Real Estate Feb. 11: What's Your Space Worth?</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">January 27, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001416">MSNBC's Ari Melber at March 10 Quarterly</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">January 26, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001415">First Judicial District Early Closing Notice</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">January 21, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001414">Sotomayor Diversity Award Nominations Due Feb. 4</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">January 12, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001413">Mock Trial Competition Judges, Coaches Needed</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">January 05, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&nnewsItemID=1001412">Nearly 1,000 Lawyers, City Officials to Greet New Chancellor January 6</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

    <p class = "row">
      <span class = "leftcol">January 05, 2015</span>
      <span class = "rightcol"><a href="http://webadmin.philadelphiabar.org/page/NewsItem?appNum=1&newsItemID=1001411">Philadelphia Bar Association Announces 2015 Officers</a>

      </span>
      <span class = "spacer">&nbsp;</span>
    </p>

</div>

</div><!-- /content area -->

</div>

<?php cb(); ?>
