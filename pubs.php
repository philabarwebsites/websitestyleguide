<?php require('chrome.php'); ct(); ?>
    
    <div class="subpage-header" data-bg="img/subheader.jpg"></div>

	<div class="row content-wrap">
		<div class="col-md-3 hidden-sm sidebar first-sidebar">
			<?php include("sidebar-left.php"); ?>
		</div> <!-- /end first sidebar -->
		
        <div class="col-md-9 col-sm-11 main-content">

    
    <a name="050505162305183"></a>
    <h1>Publications</h1>
Philadelphia Bar Association publications inform members about issues that matter to Philadelphia-area lawyers:<br>

<ul class="checkmark"><li><a target="_blank" href="http://uponfurtherreview.philadelphiabar.org/">Upon Further Review</a> is a new web publication that links lawyers to local legal news and analysis.</li><li><a href="http://philabar.org/page/ThePhiladelphiaLawyer?appNum=2">The Philadelphia Lawyer</a> is the Association's quarterly magazine devoted to the law, lifestyles and other topics of interest to Philadelphia lawyers.</li><li><a href="http://philabar.org/page/PhiladelphiaBarReporter?appNum=2">Philadelphia Bar Reporter</a> is the Association's monthly newspaper. It focuses on Association activities, member participation and services.</li><li><a href="http://philabar.org/page/BarReporterOnline?appNum=2">Bar Reporter Online</a> is the Association's bi-weekly e-newsbrief. E-mailed to Association members every Monday and Thursday mornings, it provides news and updates on Association activities, as well as the activities of legal services organizations funded by the Philadelphia Bar Foundation, between regular issues of Philadelphia Bar Reporter newspaper.</li><li><a href="http://philabar.org/page/TheLegalDirectory?appNum=2">The Legal Directory</a> lists the attorneys and firms in the five-county Philadelphia area. It also provides information about government, judiciaries, departments and agencies at the federal, state and local levels.</li></ul>
Attorneys and other professionals are invited to submit articles for consideration by the Editorial Board of <i>The Philadelphia Lawyer</i>.<p></p>
<a href="http://philabar.org/page/SubscriptionRates?appNum=2">Subscriptions</a> to <i>The Philadelphia Lawyer</i> and <i>Philadelphia Bar Reporter</i> are available to non-members.<p></p>
Information and rates on <a href="http://philabar.org/page/ThePhiladelphiaLawyerDisplayAdvertising?appNum=2">display advertising</a> are available by contacting Advertising &amp; Sales Director <a href="mailto:dchalphin@alm.com">Donald Chalphin</a> at The Legal Intelligencer at (215) 557-2359<p></p>
Comments on the publications are welcome and should be sent to <a href="mailto:mavakianhardaway@philabar.org">Meredith Z. Avakian-Hardaway</a>, Director of Communications and Marketing.
            

		</div><!-- /content area -->
		<div class="col-md-4 col-sm-5 sidebar second-sidebar">
			<?php include("sidebar-right.php"); ?>
		</div><!-- /second-sidebar -->
	</div>

<?php cb(); ?>