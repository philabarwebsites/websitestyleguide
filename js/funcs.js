$( document ).ready(function() {
 // $('.slider').fractionSlider({
	// 	'fullWidth': 			false,
	// 	'controls': 			true,
	// 	'slideComplete':		false,
	// 	'pager': 				false,
	// 	'responsive':  			true,
	// 	'dimensions':  			'1300,388',
	// 	'backgroundAnimation': 	true,
	// 	// 'nextSlideCallback': function(currentSlide) {
	//  // 		console.log(currentSlide);
	//  // 		var slide = currentSlide.find('.active-slide');
	//  // 		console.log(slide);
	// 	//  	if(slide.data('slidebg')) {
	// 	// 	 	var bg = 'url(img/' + $(slide).data('slidebg') + ')';
	// 	// 	 	console.log(bg);
	// 	// 	 	$(slide).parent().parent().parent().css({'background-image':bg})
	// 	//  	} else {
	// 	// 	 	$(slide).parent().parent().parent().css({'background-image':"none"})
	// 	//  	}
	// 	// },

	// });

var firstslidebg = $('.cycle-slideshow:first-child li').data('slide-bg');
// console.log(firstslide);
$('div.hero').css('background-image', 'url("img/' + firstslidebg + '")');

$( '.cycle-slideshow').on( 'cycle-before', function() {
	// var bg = $(this).find('.cycle-slide-active').data('slide-bg');
	$('div.hero').css('background-image', 'none');
});

$( '.cycle-slideshow').on( 'cycle-after', function() {
	var bg = $(this).find('.cycle-slide-active').data('slide-bg');
	$('div.hero').css('background-image', 'url("img/' + bg + '")');
});


$('#logintrigger').on('click',function(){
	$('.modal.login').modal();
	return false;
});

// SUBPAGE BANNERS
var subheaderbg = 'url("' + $('.subpage-header').data('bg') + '")';
$('.subpage-header').css({'background-image': subheaderbg });

$('.profiles-list').cycle();


// Mobile Nav

if($('html').hasClass('mobile')) {
	mobilemenu();
	mobilesearch();
	// console.log('phone');
}

if($('html').hasClass('tablet')) {
	mobilemenu();
	// console.log('phone');
}

$('body').on('click','.mobile-nav-trigger',function(){
togglemobilenav();
return false;
});

$('body').on('click','.mobile-nav-close',function(){
togglemobilenav();
return false;
});


function mobilemenu() {
	//console.log('i\'m a phone');
	var menu = $('.mainnav .navigation');
	var topmenu = $('.topnav .navigation');
	$('body').append('<div class="mobile-nav-wrap"></div>');
	$('.mobile-nav-wrap').prepend(menu);
	$('.mobile-nav-wrap').append(topmenu);
	$('.mobile-nav-wrap ul.list').removeClass();
	$('.mobile-nav-wrap').prepend('<a href="#" class="mobile-nav-close"><span class="fa fa-remove"></span>Close Menu</a>');
	$('.mainnav > div > div').append('<a href="#" class="mobile-nav-trigger"><span class="fa fa-bars"></span></a>');
}

function togglemobilenav() {
	if($('.mobile-nav-wrap').hasClass('open')) {
		hidemobilenav();
	} else {
		showmobilenav();
	}
}

function hidemobilenav() {
	$('.mobile-nav-wrap').animate({
		'right':'-110%'
	},500,function(){
		$('.mobile-nav-wrap').removeClass('open').hide();
		$('html').removeClass('noscroll');
	});
}

function showmobilenav() {
	$('.mobile-nav-wrap').animate({
		'right':'0%'
	},500,function(){
		$('.mobile-nav-wrap').show().addClass('open');
		$('html').addClass('noscroll');
	});
}

// Mobile Search

$('body').on('click','.mobile-search-trigger',function(){
togglemobilesearch();
return false;
});

function mobilesearch() {
	var search = $('.header .search');
	$(search).addClass('mobilesearch').insertAfter('.mainnav');
	$('.mainnav > div > div').append('<a href="#" class="mobile-search-trigger"><span class="fa fa-search"></span></a>');
}

function togglemobilesearch() {
	if($('.mobilesearch').hasClass('open')) {
		hidemobilesearch();
	} else {
		showmobilesearch();
	}
}

function hidemobilesearch() {
	$('.mobilesearch').slideUp(500,function(){
		$('.mobilesearch').removeClass('open').hide();
	});
}

function showmobilesearch() {
	$('.mobilesearch').slideDown(500,function(){
		$('.mobilesearch').addClass('open');
	});
}

// Image Captions
$('.main-content .captioned').each(function(){
	var img = $(this).find('img');
	var parent = $(img).parent();
	var pic_real_width, pic_real_height;
	$("<img/>").attr("src", $(img).attr("src")).load(function() {
        pic_real_width = this.width;   // Note: $(this).width() will not
        pic_real_height = this.height; // work for in memory images.
        console.log(parent);
        $(parent).css({'max-width': pic_real_width + 'px'});
    });
});


//Append text to all div IDs that are defaulttext;

$('.defaultText').each(function(){
	var id = 'div-' + $(this).attr('id');
	$(this).attr('id',id);
});

});
