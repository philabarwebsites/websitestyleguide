<?php require('chrome.php'); ct(); ?>
    
    <div class="subpage-header" data-bg="img/subheader.jpg"></div>

	<div class="row content-wrap">
		<div class="col-md-3 col-sm-5 hidden-xs sidebar first-sidebar">
			<?php include("sidebar-left.php"); ?>
		</div> <!-- /end first sidebar -->
		
        <div class="col-md-13 col-sm-11 main-content">

  <h1>Sample Page Title</h1>
    <p>A pba-small paragraph to <em>emphasis</em> and show <strong>important</strong> bits.</p>
    
<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros.</p>

<p>Quisque facilisis erat a dui. Nam malesuada ornare dolor. Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio. Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. Suspendisse sagittis ante a urna. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus. Donec vestibulum. Etiam vel nibh. Nulla facilisi. Mauris pharetra. Donec augue.</p>

<div class="row">
    <div class="col-md-4 blue titled outline block">
        <div class="block-inner">
            <h2 class="small title">This is a title</h2>
            <div class="content">
                This is some content
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-6 blue titled outline block">
        <div class="block-inner">
            <h2 class="small title">This is a title</h2>
            <div class="content">
                This is some content
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-4 blue titled outline block">
        <div class="block-inner">
            <h2 class="small title">This is a title</h2>
            <div class="content">
                This is some content
            </div>
        </div>
    </div>
</div>
    <ul>
        <li>This is a list item</li>
        <li>So is this - there could be more</li>
        <li>Make sure to style list items to:
            <ul>
                <li>Not forgetting child list items</li>
                <li>Not forgetting child list items</li>
                <li>Not forgetting child list items</li>
                <li>Not forgetting child list items</li>
            </ul>
        </li>
        <li>A couple more</li>
        <li>top level list items</li>
    </ul>
    <p>Don't forget <strong>Ordered lists</strong>:</p>
    <ol>
       <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
       <li>Aliquam tincidunt mauris eu risus.
        <ol>
            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
            <li>Aliquam tincidunt mauris eu risus.</li>
        </ol>
        </li>
       <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
       <li>Aliquam tincidunt mauris eu risus.
    </li></ol>
    <h2>A sub heading which is not as important as the first, but is quite imporant overall</h2>
    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
    <table>
        <tbody><tr>
            <th>Table Heading</th>
            <th>Table Heading</th>
        </tr>
        <tr>
            <td>table data</td>
            <td>table data</td>
        </tr>
        <tr>
            <td>table data</td>
            <td>table data</td>
        </tr>
        <tr>
            <td>table data</td>
            <td>table data</td>
        </tr>
        <tr>
            <td>table data</td>
            <td>table data</td>
        </tr>
    </tbody></table>    
		
    <h3>A sub heading which is not as important as the second, but should be used with consideration</h3>
    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
    <blockquote><p>“Ooh - a blockquote! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.”</p></blockquote>
    <h4>A sub heading which is not as important as the second, but should be used with consideration</h4>
    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
    <pre><code>
#header h1 a { 
    display: block; 
    width: 300px; 
    height: 80px; 
}
</code></pre>
    <h5>A sub heading which is not as important as the second, but should be used with consideration</h5>
    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
    <dl>
   <dt>Definition list</dt>
   <dd>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
	aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
	commodo consequat.</dd>
	   <dt>Lorem ipsum dolor sit amet</dt>
	   <dd>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
	aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
	commodo consequat.</dd>
	</dl>
	<h6>This heading plays a relatively pba-small bit part role, if you use it at all</h6>
    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>

 

		</div><!-- /content area -->
	</div>

<?php cb(); ?>