<?php function ct() { ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Philadelphia Bar Association</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="js/jquery-1.9.0.min.js"></script>
<!-- 	<link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.4/material.indigo-pink.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link href="stylesheets/contrib/fractionslider/fractionslider.css" media="screen, projection" rel="stylesheet" type="text/css" />
 -->
 	<!-- <link href="bootstrap/css/bootstrap.min.css" media="screen, projection" rel="stylesheet" type="text/css" /> -->
 	<!-- <link href="bootstrap/css/bootstrap-theme.min.css" media="screen, projection" rel="stylesheet" type="text/css" /> -->
 	<link href="stylesheets/screen.css" media="screen, projection" rel="stylesheet" type="text/css" />
  <style media="screen">
    .cle a {
      background-color: #CCCCCC !important;
    }
    .cle:before {
      content:"cle";
      padding:10px;
      margin:20px;
      border-bottom: red solid 4px;
    }
  </style>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="header">
		<div class="row">
			<div class="col-md-16 tright">
				<div class="topnav">
					<ul class="pba-small strong horizontal piped navigation list">
						<li><a href="#">About the Bar</a></li>
						<li><a href="#">Staff Directory</a></li>
						<li><a href="#">Employment Opportunities</a></li>
						<li><a href="#">Contact Us</a></li>
						<li><a href="#">Member Login</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-sm-8">
				<div class="logo">
					<a href="home.php"><img src="img/logo.png"></a>
					<div class="follow">
						<ul class="horizontal list">
							<li class="text">Follow us on:</li>
							<li><a href="#"><span class="fa fa-twitter"></span></a></li>
							<li><a href="#"><span class="fa fa-facebook"></span></a></li>
							<li><a href="#"><span class="fa fa-linkedin"></span></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-sm-8 tright">
				<div class="search">
					<form>
						<input type="text" placeholder="enter search term"><input type="submit" value="search" class="pba-small blue button">
					</form>
				</div>
			</div>
		</div>
	</div><!-- /header -->


	<div class="mainnav">
		<div class="row content-wrap">
			<div class="col-md-16">
				<ul class="large horizontal strong dropdown navigation list">
					<li><a href="#">For Members</a>
						<ul>
              <li><a href="#">Join/Renew</a></li>
              <li><a href="#">Membership Brochure</a></li>
              <li><a href="#">Sections and Committees</a></li>
              <li><a href="#">Diversity and Inclusion Action Plan</a></li>
						</ul>
					</li>
          <li><a href="#">CLE</a>
            <ul>
              <li><a href="#">Speaker Resources</a></li>
              <li><a href="#">CLE Calendar</a></li>
            </ul>
          </li>
					<li><a href="#">For the Public</a>
            <ul>
              <li><a href="#">Online Lawyer Referral</a></li>
              <li><a href="#">Fee Disputes</a></li>
              <li><a href="#">Community Outreach</a></li>
              <li><a href="#">Speakers Bureau</a></li>
            </ul>
          </li>
					<!-- <li><a href="#">CLEs</a></li> -->
          <li><a href="news2.php">Publications</a>
            <ul>
              <li><a href="#">Upon Further Review</a></li>
              <li><a href="#">Philadelphia Bar Reporter</a></li>
              <li><a href="#">The Philadelphia Lawyer</a></li>
              <li><a href="#">The Legal Directory</a></li>
              <li><a href="#">Bar Reporter Online</a></li>
              <li><a href="#">Young Lawyers Division Ezine</a></li>
            </ul>
          </li>
					<li><a href="policy.php">Policy and Leadership</a></li>
					<li><a href="newslist.php">News</a></li>
					<li><a href="calendar.php">Calendar</a></li>
					<li><a href="#">Career Center</a></li>
				</ul>
			</div>
		</div>
	</div><!-- /mainnav -->
	<div class="contentbg">

<?php }
function cb() { ?>


	</div><!-- /contentbg -->
	<div class="footer">
		<div class="row content-wrap">
			<div class="col-md-16">
				<img src="img/pba-footer-logo.png" class="footer-logo">
				<p>&copy; 2015 Philadelphia Bar Association. All Rights Reserved.</p>
				<p>1101 Market Street, 11th Floor &bull; Philadelphia, PA 19107 &bull; Phone: (215) 238-6300 &bull; Fax: (215) 238-1159</p>
				<ul class="pba-small horizontal piped navigation list">
					<li><a href="#">Privacy Policy </a></li>
					<li><a href="#">Confidentiality Notice </a></li>
					<li><a href="#">Disclaimer </a></li>
					<li><a href="#">Contact Us</a></li>
					<li><a href="#">Send Us Your Feedback!</a></li>
				</ul>
			</div>
		</div>
	</div><!-- /footer -->

<script src="bootstrap/js/bootstrap.min.js"></script>

<!-- <script src="https://storage.googleapis.com/code.getmdl.io/1.0.4/material.min.js"></script> -->
<!-- <script type="text/javascript" src="js/contrib/fractionslider/jquery.fractionslider.min.js"></script> -->
<script type="text/javascript" src="js/contrib/device/device.js"></script>
<script type="text/javascript" src="js/contrib/cycle2/jquery.cycle2.min.js"></script>
<script type="text/javascript" src="js/contrib/cycle2/jquery.cycle2.swipe.min.js"></script>
<script type="text/javascript" src="js/funcs.js"></script>

</body>
</html>


 <?php } ?>
