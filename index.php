<?php require('chrome.php'); ct(); ?>
    
    <div class="subpage-header" data-bg="img/subheader.jpg"></div>

	<div class="row content-wrap">
		<div class="col-md-3 hidden-sm sidebar first-sidebar">
			<?php include("sidebar-left.php"); ?>
		</div> <!-- /end first sidebar -->
		
        <div class="col-md-9 col-sm-11 main-content">

  <h1>Philadelphia Bar Association - Style Guide</h1>

  <div class="nav-grid">
    <?php include('sidenav.php'); ?>
    </div>
    
 

		</div><!-- /content area -->
		<div class="col-md-4 col-sm-5 sidebar second-sidebar">
			<?php include("sidebar-right.php"); ?>
		</div><!-- /second-sidebar -->
	</div>

<?php cb(); ?>