<?php require('chrome.php'); ct(); ?>
    
    <div class="subpage-header" data-bg="img/subheader.jpg"></div>

	<div class="row content-wrap">
		<div class="col-md-3 hidden-sm sidebar first-sidebar">
			<?php include("sidebar-left.php"); ?>
		</div> <!-- /end first sidebar -->
		
        <div class="col-md-9 col-sm-11 main-content">
            <h1>Images</h1>
            <p>Images can be left with no alignment which renders them centered between paragraphs or left or right aligned with the text wrapping around the image element.</p>
            <hr>
            <a name="alig"></a><h2>Alignment</h2>
            <h3>Default</h3>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <img src="img/content-image-large.jpg" class="center aligned">
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <hr>
            <h3>Left</h3>
            <div class="note">Add class: left to the img tag</div>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <img src="img/content-image-medium.jpg" class="left aligned">
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <hr>
            <h3>Right</h3>
            <div class="note">Add class: right to the img tag</div>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <img src="img/content-image-small.jpg" class="right aligned">
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>

            <a name="capt"></a><h2>Captioned</h2>
            <div class="note">Ensure image height and width are set as the image is intended to be displayed. Don't resize the image using css or image size tags. Add the structure below to add a captioned image to the content area:</div>
            <pre><code>
&lt;div class=&quot;captioned image&quot;&gt;
    &lt;img src=&quot;img/content-image.jpg&quot;&gt;
    &lt;div class=&quot;caption&quot;&gt;
        Caption text here. Caption text here. Caption text here.
    &lt;/div&gt;
&lt;/div&gt;
            </code></pre>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <div class="captioned image left aligned">
                <img src="img/content-image-small.jpg">
                <div class="caption">
                    Caption text here. Caption text here. Caption text here.
                </div>
            </div>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <hr>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <div class="captioned image center aligned">
                <img src="img/content-image-large.jpg">
                <div class="caption">
                    <p>Caption text here. Caption text here. Caption text here.</p>
                    <p>Caption text here. Caption text here. Caption text here.</p>
                </div>
            </div>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
		<a name="subh"></a><h2>Image Sub Headers</h2>
        <div class="note">
            Image sub headers are images that span the width of the content area and can include a centerted call to action message. Add the following structure to create an Image Sub Header:
        </div>
        <pre><code>
&lt;div class=&quot;subheader&quot;&gt;             
    &lt;img src=&quot;img/content-image-short.jpg&quot;&gt;         
&lt;/div&gt;
        </code></pre>
        
        <hr>
        
        <div class="subheader">
            <img src="img/content-image-short.jpg">
        </div>
        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>        
        <hr>
        
        <h3>Dark Image, Light Text</h3>
        <pre><code>
&lt;div class=&quot;dark centered subheader&quot;&gt;
    &lt;img src=&quot;img/content-image-large-dark.jpg&quot;&gt;
    &lt;div class=&quot;text&quot;&gt;
        &lt;h2&gt;This is a large chunk of text&lt;/h2&gt;
        &lt;p&gt;This is some message in smaller text.&lt;/p&gt;
        &lt;a href=&quot;#&quot; class=&quot;blue button&quot;&gt;Call to Action link&lt;/a&gt;
    &lt;/div&gt;
&lt;/div&gt;
        </code></pre>
        <div class="dark centered subheader"> <img src="img/content-image-large-dark.jpg"> 
            <div class="text"> 
                <h2>This is a large chunk of text</h2> 
                <p>This is some message in smaller text.</p> 
                <a href="#" class="blue button">Call to Action link</a> 
            </div>
        </div>
        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p> 
        <hr>
        <h3>Light Image, Dark Text</h3>
        <pre><code>
            &lt;div class=&quot;light centered subheader&quot;&gt;
                &lt;img src=&quot;img/content-image-large-light.jpg&quot;&gt;
                &lt;div class=&quot;text&quot;&gt;
                    &lt;h2&gt;This is a large chunk of text&lt;/h2&gt;
                    &lt;p&gt;This is some message in smaller text.&lt;/p&gt;
                    &lt;a href=&quot;#&quot; class=&quot;blue button&quot;&gt;Call to Action link&lt;/a&gt;
                &lt;/div&gt;
            &lt;/div&gt;
        </code></pre>        
        <div class="light centered subheader">
            <img src="img/content-image-large-light.jpg">
            <div class="text">
                <h2>This is a large chunk of text</h2>
                <p>This is some message in smaller text.</p>
                <a href="#" class="blue button">Call to Action link</a>
            </div>
        </div>
        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>           

        </div><!-- /content area -->

		<div class="col-md-4 col-sm-5 sidebar second-sidebar">
			<?php include('sidebar-right.php'); ?>
		</div><!-- / sidebar-second -->
	</div>

<?php cb(); ?>