<?php require('chrome.php'); ct(); ?>
    
    <div class="subpage-header" data-bg="img/subheader.jpg"></div>

	<div class="row content-wrap">
		<div class="col-md-3 hidden-sm sidebar first-sidebar">
			<?php include("sidebar-left.php"); ?>
		</div> <!-- /end first sidebar -->
		
        <div class="col-md-9 col-sm-11 main-content">
            <h1>Blocks</h1>
            <p>Blocks will always stretch to the width of their container. For example the sidebar blocks will always be as wide as the sidebar elements. If you intend to use these block styles within the content, be sure to add the appropriate Bootstrap grid markup and append the formatting styles below to change the look of the blocks.</p>
            <hr>
            <a name="defa"></a><h2>Default Block Structure</h2>
            <div class="note">This code simply defines a block element as such with the default styles applied.</div>
            <pre><code>
&lt;div class=&quot;block&quot;&gt; 
    &lt;div class=&quot;block-inner&quot;&gt; 
        &lt;h3 class=&quot;title&quot;&gt;Block Title&lt;/h3&gt; 
        &lt;div class=&quot;content&quot;&gt; 
            &lt;p&gt;Quisque facilisis erat a dui. Nam malesuada ornare dolor.&lt;/p&gt; 
            &lt;p&gt;&lt;a href=&quot;#&quot; class=&quot;pba-small grey button&quot;&gt;Download&lt;/a&gt;&lt;/p&gt; 
        &lt;/div&gt; 
    &lt;/div&gt; 
&lt;/div&gt;
            </code></pre>
            <div class="content-block-container">

                <div class="block">
                    <div class="block-inner">
                        <h3 class="title">Block Title</h3>
                        <div class="content">
                            <p>Quisque facilisis erat a dui. Nam malesuada ornare dolor.</p>
                            <p><a href="#" class="pba-small grey button">Download</a></p>
                        </div>
                    </div>
                </div><!-- /block -->

            </div><!-- /block-container -->

            <hr>
            <h2>Block Styles</h2>
            <p>By adding intuitive classes to the .block elements, we can change the style. Below are the available styles with examples and code.</p>
            <hr>
            <a name="soli"></a><h3>Solid Color Blocks</h3> 
            <div class="note">Add the class: solid along with a color (blue, orange, green) to change the background color of the block. Omit a color to render a grey block.</div>
            <pre><code>
&lt;div class=&quot;solid blue block&quot;&gt; 
    &lt;div class=&quot;block-inner&quot;&gt; 
        &lt;h3 class=&quot;title&quot;&gt;Block Title&lt;/h3&gt; 
        &lt;div class=&quot;content&quot;&gt; 
            &lt;p&gt;Quisque facilisis erat a dui. Nam malesuada ornare dolor.&lt;/p&gt; 
            &lt;p&gt;&lt;a href=&quot;#&quot; class=&quot;pba-small grey button&quot;&gt;Download&lt;/a&gt;&lt;/p&gt; 
        &lt;/div&gt; 
    &lt;/div&gt; 
&lt;/div&gt;
            </code></pre>
            <div class="content-block-container">

                <div class="solid blue block">
                    <div class="block-inner">
                        <h3 class="title">Block Title</h3>
                        <div class="content">
                            <p>Quisque facilisis erat a dui. Nam malesuada ornare dolor.</p>
                            <p><a href="#" class="pba-small grey button">Download</a></p>
                        </div>
                    </div>
                </div><!-- /block -->

            <div class="solid block">
                <div class="block-inner">
                    <h3 class="title">Block Title</h3>
                    <div class="content">
                        <p>Quisque facilisis erat a dui. Nam malesuada ornare dolor.</p>
                        <p><a href="#" class="pba-small grey button">Download</a></p>
                    </div>
                </div>
            </div><!-- /block -->

        </div><!-- /block-container -->


        <hr>
            <a name="outl"></a><h3>Outline Blocks</h3> 
            <div class="note">Add the class: outline along with a color (blue, orange, green) to add a border of the chosen color to the block. Omit a color to render a grey block.</div>
            <pre><code>
&lt;div class=&quot;blue outline block&quot;&gt; 
    &lt;div class=&quot;block-inner&quot;&gt; 
        &lt;h3 class=&quot;title&quot;&gt;Block Title&lt;/h3&gt; 
        &lt;div class=&quot;content&quot;&gt; 
            &lt;p&gt;Quisque facilisis erat a dui. Nam malesuada ornare dolor.&lt;/p&gt; 
            &lt;p&gt;&lt;a href=&quot;#&quot; class=&quot;pba-small grey button&quot;&gt;Download&lt;/a&gt;&lt;/p&gt; 
        &lt;/div&gt; 
    &lt;/div&gt; 
&lt;/div&gt;
            </code></pre>
            <div class="content-block-container">

                <div class="blue outline block">
                    <div class="block-inner">
                        <h3 class="title">Block Title</h3>
                        <div class="content">
                            <p>Quisque facilisis erat a dui. Nam malesuada ornare dolor.</p>
                            <p><a href="#" class="pba-small grey button">Download</a></p>
                        </div>
                    </div>
                </div><!-- /block -->

        </div><!-- /block-container -->

        <hr>
            <a name="outlt"></a><h3>Outline Titled Blocks</h3> 
            <div class="note">Add the classes: titled outline along with a color (blue, orange, green) to add a border and title background of the chosen color to the block. Omit a color to render a grey block.</div>
            <pre><code>
&lt;div class=&quot;blue titled outline block&quot;&gt; 
    &lt;div class=&quot;block-inner&quot;&gt; 
        &lt;h3 class=&quot;title&quot;&gt;Block Title&lt;/h3&gt; 
        &lt;div class=&quot;content&quot;&gt; 
            &lt;p&gt;Quisque facilisis erat a dui. Nam malesuada ornare dolor.&lt;/p&gt; 
            &lt;p&gt;&lt;a href=&quot;#&quot; class=&quot;pba-small grey button&quot;&gt;Download&lt;/a&gt;&lt;/p&gt; 
        &lt;/div&gt; 
    &lt;/div&gt; 
&lt;/div&gt;
            </code></pre>
            <div class="content-block-container">

                <div class="orange titled outline block">
                    <div class="block-inner">
                        <h3 class="title">Block Title</h3>
                        <div class="content">
                            <p>Quisque facilisis erat a dui. Nam malesuada ornare dolor. Quisque facilisis erat a dui. Nam malesuada ornare dolor. Quisque facilisis erat a dui. Nam malesuada ornare dolor.</p>
                            <p><a href="#" class="pba-small orange button">Download</a></p>
                        </div>
                    </div>
                </div><!-- /block -->

        </div><!-- /block-container -->

        <hr>
            <a name="imag"></a><h3>Image Blocks</h3> 
            <div class="note">Add the class: image and an alignment (left or right) to add accomade and align an image added to the block. Follow the structure illustrated below for best results. You can also add a color class to change the color of the block.</div>
            <pre><code>
&lt;div class=&quot;blue left image block&quot;&gt; 
    &lt;div class=&quot;block-inner&quot;&gt; 
        &lt;h3 class=&quot;title&quot;&gt;Block Title&lt;/h3&gt; 
        &lt;div class=&quot;content&quot;&gt; 
            &lt;p&gt;Quisque facilisis erat a dui. Nam malesuada ornare dolor.&lt;/p&gt; 
            &lt;p&gt;&lt;a href=&quot;#&quot; class=&quot;pba-small grey button&quot;&gt;Download&lt;/a&gt;&lt;/p&gt; 
        &lt;/div&gt; 
    &lt;/div&gt; 
&lt;/div&gt;
            </code></pre>
            <div class="content-block-container">

                <div class="blue left image block">
                    <div class="block-inner">
                        <h3 class="title">Block Title</h3>
                        <img src="img/thumb-top10.jpg">
                        <div class="content">
                            <p>Quisque facilisis erat a dui. Nam malesuada ornare dolor. Quisque facilisis erat a dui. Nam malesuada ornare dolor. Quisque facilisis erat a dui. Nam malesuada ornare dolor.</p>
                            <p><a href="#" class="pba-small orange button">Download</a></p>
                        </div>
                    </div>
                </div><!-- /block -->

                <div class="blue right image block">
                    <div class="block-inner">
                        <h3 class="title">Block Title</h3>
                        <img src="img/thumb-top10.jpg">
                        <div class="content">
                            <p>Quisque facilisis erat a dui. Nam malesuada ornare dolor. Quisque facilisis erat a dui. Nam malesuada ornare dolor. Quisque facilisis erat a dui. Nam malesuada ornare dolor.</p>
                            <p><a href="#" class="pba-small orange button">Download</a></p>
                        </div>
                    </div>
                </div><!-- /block -->
                
        </div><!-- /block-container -->  
                <hr>
                <h3>Combined Classes Examples</h3>
            <div class="content-block-container">

                <div class="note">Has the following classes:<br>blue titled outline right image block</div>
                <div class="blue titled outline right image block">
                    <div class="block-inner">
                        <h3 class="title">Block Title</h3>
                        <img src="img/thumb-top10.jpg">
                        <div class="content">
                            <p>Quisque facilisis erat a dui. Nam malesuada ornare dolor. Quisque facilisis erat a dui. Nam malesuada ornare dolor. Quisque facilisis erat a dui. Nam malesuada ornare dolor.</p>
                            <p><a href="#" class="pba-small grey button">Download</a></p>
                        </div>
                    </div>
                </div><!-- /block -->

                <div class="note">Has the following classes:<br>green titled outline left image block</div>
                <div class="green titled outline left image block">
                    <div class="block-inner">
                        <h3 class="title">Block Title</h3>
                        <img src="img/thumb-top10.jpg">
                        <div class="content">
                            <p>Quisque facilisis erat a dui. Nam malesuada ornare dolor. Quisque facilisis erat a dui. Nam malesuada ornare dolor. Quisque facilisis erat a dui. Nam malesuada ornare dolor.</p>
                            <p><a href="#" class="pba-small grey button">Download</a></p>
                        </div>
                    </div>
                </div><!-- /block -->                


        </div><!-- /block-container -->  



        </div><!-- /content area -->

		<div class="col-md-4 col-sm-5 sidebar second-sidebar">
			<?php include('sidebar-right.php'); ?>
		</div><!-- / sidebar-second -->
	</div>

<?php cb(); ?>