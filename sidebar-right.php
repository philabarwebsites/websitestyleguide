<div class="solid green block">
	<div class="block-inner">
		<h3 class="title">This is a block title</h3>
		<div class="content">
			This is some content
		</div>
	</div>
</div><!-- /block -->

<div class="solid green centered block">
	<div class="block-inner">
	<h3 class="medium title">Renew Your Membership</h3>
		<a href="#" class="pba-small semitransparent grey button">Some link text here</a>
	</div>
</div><!-- /block -->

<div class="solid blue single link block">
	<div class="block-inner">
		<a href="#">Some link text here</a>
	</div>
</div><!-- /block -->

<div class="solid orange single link block">
	<div class="block-inner">
		<a href="#">Some link text here</a>
	</div>
</div><!-- /block -->

<div class="plain left image orange block">
    <div class="block-inner">
        <img src="img/thumb-top10.jpg">
        <h3 class="small bold title">Block Title</h3>
        <div class="content">
            <p>Quisque facilisis erat a dui. Nam malesuada ornare dolor.</p>
            <p><a href="#" class="pba-small grey button">Download</a></p>
        </div>
    </div>
</div><!-- /block -->

<div class="plain left image blue block">
	<div class="block-inner">
		<img src="img/thumb-top10.jpg">
		<h3 class="small bold title">Block Title</h3>
		<div class="content">
            <p>Quisque facilisis erat a dui. Nam malesuada ornare dolor.</p>
			<p><a href="#" class="pba-small grey button">Download</a></p>
		</div>
	</div>
</div><!-- /block -->

<div class="orange titled outline block">
    <div class="block-inner">
        <h3 class="medium title">Renew Your Membership</h3>
        <div class="content">
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <a href="#" class="pba-small semitransparent grey button">Some link text here</a>
        </div>
    </div>
</div><!-- /block -->

<div class="blue titled outline block">
    <div class="block-inner">
        <h3 class="medium title">Renew Your Membership</h3>
        <div class="content">
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <a href="#" class="pba-small semitransparent grey button">Some link text here</a>
        </div>
    </div>
</div><!-- /block -->

<div class="green titled outline block">
    <div class="block-inner">
        <h3 class="medium title">Renew Your Membership</h3>
        <div class="content">
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
            <a href="#" class="pba-small semitransparent grey button">Some link text here</a>
        </div>
    </div>
</div><!-- /block -->