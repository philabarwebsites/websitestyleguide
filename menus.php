<?php require('chrome.php'); ct(); ?>
    
    <div class="subpage-header" data-bg="img/subheader.jpg"></div>

	<div class="row content-wrap">
		<div class="col-md-3 hidden-sm sidebar first-sidebar">
			<?php include("sidebar-left.php"); ?>
		</div> <!-- /end first sidebar -->
		
        <div class="col-md-9 col-sm-11 main-content">

  <h1>Menus</h1>


<p>Standard Horizontal Menu element. These can be used as anchor layouts within pages provided the number of items does not exceed the width of the page.</p>

<hr>

<ul class="horizontal navigation list">
	<li><a href="#">Menu Item</a></li>
	<li><a href="#">Menu Item</a></li>
	<li><a href="#">Menu Item</a></li>
	<li><a href="#">Menu Item</a></li>
	<li><a href="#">Menu Item</a></li>
</ul>
    
 

		</div><!-- /content area -->
		<div class="col-md-4 col-sm-5 sidebar second-sidebar">
			<?php include("sidebar-right.php"); ?>
		</div><!-- /second-sidebar -->
	</div>

<?php cb(); ?>