<?php require('chrome.php'); ct(); ?>

    <div class="subpage-header" data-bg="img/subheader.jpg"></div>

	<div class="row content-wrap">
        <div class="col-md-16 main-content">

<h1>Calendar</h1>
<img alt="RSVP" border="0" hspace="3" src="img/reply.gif"> = Events requiring an RSVP or registration.
<div id="_calendarContainer" data-updateurl="/cgi-bin/WebObjects/PBAReadOnly.woa/ajax/7.0.29.11.CalendarWidgetComponent.2">
<form name="f_0_29_11_CalendarWidgetComponent_2_1" method="post" action="/cgi-bin/WebObjects/PBAReadOnly.woa/wo/7.0.29.11.CalendarWidgetComponent.2.1">
	<center>
    <table cellpadding="0" cellspacing="0" id="calendar">
    			<tbody><tr id="title">
    				<th colspan="2" id="thismonth">October 2015</th>
    				<th colspan="3">
    				  <a href="/cgi-bin/WebObjects/PBAReadOnly.woa/wo/0.0.29.11.CalendarWidgetComponent.2.1.3">«previous</a>&nbsp;
    				  <select onchange="form.submit();" name="0.29.11.CalendarWidgetComponent.2.1.5"><option value="0">July 2015</option><option value="1">August 2015</option><option value="2">September 2015</option><option selected="selected" value="3">October 2015</option><option value="4">November 2015</option><option value="5">December 2015</option><option value="6">January 2016</option><option value="7">February 2016</option><option value="8">March 2016</option><option value="9">April 2016</option><option value="10">May 2016</option><option value="11">June 2016</option></select>&nbsp;
    				  <a href="/cgi-bin/WebObjects/PBAReadOnly.woa/wo/0.0.29.11.CalendarWidgetComponent.2.1.7">next»</a>
    				</th>
    				<th colspan="2" align="right">
    					<a href="webcal://www.philadelphiabar.org/cgi-bin/WebObjects/PBAReadOnly.woa/wa/calendar?includeSubcommittees=true">subscribe</a> |
    					 <a href="http://www.philadelphiabar.org/cgi-bin/WebObjects/PBAReadOnly.woa/wa/calendar?includeSubcommittees=true">download</a>
    				</th>
    			</tr>

    			<tr id="days">
    				<th>Sun</th>
    				<th>Mon</th>
    				<th>Tue</th>
    				<th>Wed</th>
    				<th>Thu</th>
    				<th>Fri</th>
    				<th>Sat</th>
    			</tr>


    				<tr>

    					<td width="14%" valign="top" class="col1 priormonth sun"><div class="date">27</div>
    						<div class="cell">

    						</div>
    					</td>

    					<td width="14%" valign="top" class="priormonth mon"><div class="date">28</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=PAPAL152">Offices Closed</a>


    								<div class="popstyle" id="dekPAPAL152" >




    											<b>Time:</b>
    											<br>
    											08:30 AM

    												to
    												05:00 PM





    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="priormonth tue"><div class="date">29</div>
    						<div class="cell">

    								<div class="title cle">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=BA092915">Vatican Splendors a Bar Academy Event</a>


    								<div class="popstyle" id="dekBA092915">




    											<b>Time:</b>
    											<br>
    											06:00 PM

    												to
    												08:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									The Franklin Institute<br>222 N. 20th St.<br><br>Philadelphia, PA 19103
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=WIP929">Women in the Profession Committee Meeting</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekWIP929" >




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>10th Floor Board Room<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=INTL0929">International Business Initiative Committee Meeting</a>

    									<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekINTL0929">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Committee Room South<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="priormonth wed"><div class="date">30</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=ENVIR0930">Environmental &amp; Energy Law Committee Meeting</a>

    									<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekENVIR0930">




    											<b>Time:</b>
    											<br>
    											04:00 PM

    												to
    												06:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Conference Center<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title cle">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=IRV15">Application Period for Irvin Stander Award Begins</a>


    								<div class="popstyle" id="dekIRV15">



    											<b>All Day</b>





    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=CUS0930">Custody Committee Meeting</a>


    								<div class="popstyle" id="dekCUS0930">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Obermayer Rebmann Maxwell &amp; Hippel LLP<br>One Penn Center, 19th Floor<br>1617 JFK Blvd.<br>Philadelphia, PA 19103
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=LGBT930">LGBT Rights Committee CLE</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekLGBT930">




    											<b>Time:</b>
    											<br>
    											12:30 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Conference Center<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth thu"><div class="date">1</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=LFL1001">Law Firm Laboratory</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekLFL1001">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>10th Floor Board Room<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=LRIS1001">LRIS Committee</a>


    								<div class="popstyle" id="dekLRIS1001">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Committee Room South<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    							<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=BRE1001">Bar Reporter Editorial Board</a>


    								<div class="popstyle" id="dekBRE1001">




    											<b>Time:</b>
    											<br>
    											08:30 AM

    												to
    												10:30 AM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Committee Room South<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth fri"><div class="date">2</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=CRC1002">Civil Rights Committee Meeting</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekCRC1002">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Conference Center<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="col7 thismonth sat"><div class="date">3</div>
    						<div class="cell">

    						</div>
    					</td>

    				</tr>

    				<tr>

    					<td width="14%" valign="top" class="col1 thismonth sun"><div class="date">4</div>
    						<div class="cell">

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth mon"><div class="date">5</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=MC1005">Municipal Court Committee Meeting</a>


    								<div class="popstyle" id="dekMC1005">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Municipal Court<br>1339 Chestnut Street<br>10th Floor Conference Room
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=FLS1005">Family Law Section Monthly Meeting</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekFLS1005">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Conference Center<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth tue"><div class="date">6</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=PRO1006">Probate and Trust Section Quarterly Meeting and CLE</a>


    								<div class="popstyle" id="dekPRO1006">




    											<b>Time:</b>
    											<br>
    											12:30 PM

    												to
    												02:30 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Pennsylvania Bar Institute<br>CLE Conference Center<br>Wanamaker Building, 10th Floor<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=LRP1005">RESCHEDULED Heather Jarvis Presentation</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekLRP1005">




    											<b>Time:</b>
    											<br>
    											03:00 PM

    												to
    												05:30 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Conference Center<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=LRPWD1006">Legal Rights of Persons With Disabilities Committee Meeting</a>

    										<img alt="RSVP" border="0" hspace="3" src="http://www.philadelphiabar.org/WebObjects/PBAReadOnly.woa/Contents/WebServerResources/CMSResources/images/cal/reply.gif">


    								<div class="popstyle" id="dekLRPWD1006">




    											<b>Time:</b>
    											<br>
    											09:00 AM

    												to
    												10:30 AM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>10th Floor Board Room<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=IMM917">CANCELLED: Immigration Law Committee Meeting</a>


    								<div class="popstyle" id="dekIMM917">



    											<b>All Day</b>





    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth wed"><div class="date">7</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=WRC1006">Women's Rights Committee Meeting</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekWRC1006">




    											<b>Time:</b>
    											<br>
    											04:00 PM

    												to
    												06:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Marshall Dennehey Warner Coleman &amp; Goggin<br>2000 Market Street<br>Suite 2300<br>Philadelphia, PA 19103
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=FRAN1007">Franchise Law Committee Meeting</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekFRAN1007">




    											<b>Time:</b>
    											<br>
    											12:30 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Conference Center<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=ADRFLS1007">CANCELLED: ADR Committee of the Family Law Section</a>


    								<div class="popstyle" id="dekADRFLS1007">



    											<b>All Day</b>





    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=CJSEXE1007">Criminal Justice Section Executive Committee Meeting</a>


    								<div class="popstyle" id="dekCJSEXE1007">




    											<b>Time:</b>
    											<br>
    											12:30 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Committee Room South<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    							<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=DLSC1007">Delivery of Legal Services Committee</a>


    								<div class="popstyle" id="dekDLSC1007">




    											<b>Time:</b>
    											<br>
    											08:30 AM

    												to
    												10:30 AM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>10th Floor Board Room<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth thu"><div class="date">8</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=EOM1008">City of Philadelphia OEM</a>


    								<div class="popstyle" id="dekEOM1008">




    											<b>Time:</b>
    											<br>
    											08:30 AM

    												to
    												11:30 AM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Conference Center<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=LEGIS1008">Legislative Liaison Committee Meeting</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekLEGIS1008">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>10th Floor Board Room<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth fri"><div class="date">9</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=WCSEXE1009">CANCELLED: Workers' Compensation Section Executive Committee</a>


    								<div class="popstyle" id="dekWCSEXE1009">



    											<b>All Day</b>





    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=WCS1009">CANCELLED: Workers' Compensation Section CLE Program</a>


    								<div class="popstyle" id="dekWCS1009">



    											<b>All Day</b>





    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=INTL1009">Global Philly 2015 &amp;  International Law Committee Program</a>


    								<div class="popstyle" id="dekINTL1009">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												01:30 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Pepper Hamilton LLP<br>3000 Two Logan Square<br>Eighteenth and Arch Street
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=PUBLIC1009">UKNITE</a>

    									<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekPUBLIC1009">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												01:30 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Conference Center<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=GOVT1001">Government and Public Service Lawyers Committee Meeting</a>

    									<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekGOVT1001">




    											<b>Time:</b>
    											<br>
    											03:30 PM

    												to
    												05:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Committee Room<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=PLE1009">CANCELLED: Philadelphia Lawyer Editorial Board</a>


    								<div class="popstyle" id="dekPLE1009">



    											<b>All Day</b>





    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="col7 thismonth sat"><div class="date">10</div>
    						<div class="cell">

    						</div>
    					</td>

    				</tr>

    				<tr>

    					<td width="14%" valign="top" class="col1 thismonth sun"><div class="date">11</div>
    						<div class="cell">

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth mon"><div class="date">12</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=COLO15">Columbus Day - Offices Closed</a>


    								<div class="popstyle" id="dekCOLO15">




    											<b>Time:</b>
    											<br>
    											08:30 AM

    												to
    												05:00 PM





    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth tue"><div class="date">13</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=APP1013">Appellate Courts Committee Meeting</a>

    									<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekAPP1013">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												01:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Conference Center<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=ORPHAN1013">Orphan's Court Litigation &amp; Dispute Resolution</a>


    								<div class="popstyle" id="dekORPHAN1013">




    											<b>Time:</b>
    											<br>
    											08:30 AM

    												to
    												10:30 AM




    										<br>
    										<b>Location:</b>
    										<br>

    									HQ Business Center<br>1500 Market Street<br>East Tower, 12th Floor<br>Philadelphia, PA 19102
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=RULE1013">Rules and Practice Committee of the Probate Section</a>


    								<div class="popstyle" id="dekRULE1013">




    											<b>Time:</b>
    											<br>
    											04:00 PM

    												to
    												06:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Schachtel, Gerstley, Levin &amp; Koplin, P.C.<br>123 South Broad Street<br>Suite 2170<br>Philadelphia, PA 19109
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=RPE1013">Real Property Executive Committee Meeting</a>

    								<div class="popstyle" id="dekRPE1013">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Ballard Spahr LLP<br>1735 Market Street<br>
    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth wed"><div class="date">14</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=INTL1014">Recent Developments and Trends in Asylum Law</a>


    								<div class="popstyle" id="dekINTL1014">




    											<b>Time:</b>
    											<br>
    											04:30 PM

    												to
    												06:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Conference Center
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=BRAN1014">Brandeis Law Society</a>


    								<div class="popstyle" id="dekBRAN1014">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>10th Floor Board Room<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=INTPR1014">CANCELLED: Intellectual Property Committee Meeting</a>


    								<div class="popstyle" id="dekINTPR1014">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM





    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth thu"><div class="date">15</div>
    						<div class="cell">

    							<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=ELD1015">CANCELLED: Elder Law &amp; Guardianship Committee Meeting</a>


    								<div class="popstyle" id="dekELD1015">



    											<b>All Day</b>





    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=CRC15">Civil Rights Committee Meeting</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekCRC15">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>10th Floor Board Room<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=FLSEXE1015">Family Law Section Executive Committee</a>


    								<div class="popstyle" id="dekFLSEXE1015">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Committee Room South<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth fri"><div class="date">16</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=BB101515">2015 Bench-Bar and Annual Conference</a>


    								<div class="popstyle" id="dekBB101515">

    										<b>Date/Time:</b>
    										<br>
    										10/16/2015 08:00 AM to 10/17/2015 02:00 PM



    										<br>
    										<b>Location:</b>
    										<br>

    									Borgata<br>1 Borgata Way<br><br>Atlantic City, NJ 08401
    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="col7 thismonth sat"><div class="date">17</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=BB101515">2015 Bench-Bar and Annual Conference</a>


    								<div class="popstyle" id="dekBB101515">

    										<b>Date/Time:</b>
    										<br>
    										10/16/2015 08:00 AM to 10/17/2015 02:00 PM



    										<br>
    										<b>Location:</b>
    										<br>

    									Borgata<br>1 Borgata Way<br><br>Atlantic City, NJ 08401
    									<br>

    								</div></div>

    						</div>
    					</td>

    				</tr>

    				<tr>

    					<td width="14%" valign="top" class="col1 thismonth sun"><div class="date">18</div>
    						<div class="cell">

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth mon"><div class="date">19</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=CJS1019">Criminal Justice Section CLE</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekCJS1019">




    											<b>Time:</b>
    											<br>
    											12:30 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Conference Center<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=PUBLIC1019">Public Interest Executive Committee Monthly Meeting</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekPUBLIC1019">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>10th Floor Board Room<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth tue"><div class="date">20</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=ED1020">Education Committee of the Probate and Trust Law Section</a>


    								<div class="popstyle" id="dekED1020">




    											<b>Time:</b>
    											<br>
    											04:00 PM

    												to
    												05:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Flaster/Greenberg<br>Four Penn Center<br>1600 JFK Blvd, 2nd Floor<br>Philadelphia, PA 19103
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=BL1020">Business Litigation Committee Meeting</a>

    									<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekBL1020">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Conference Center<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=EMP1020">Employee Benefits Committee Meeting</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekEMP1020">




    											<b>Time:</b>
    											<br>
    											12:30 PM

    												to
    												02:30 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Committee Room South<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=CAB1020">Cabinet</a>


    								<div class="popstyle" id="dekCAB1020">




    											<b>Time:</b>
    											<br>
    											01:00 PM

    												to
    												03:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>10th Floor Board Room<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth wed"><div class="date">21</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=BLE1021">Business Law Executive Committee Meeting</a>


    								<div class="popstyle" id="dekBLE1021">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Dilworth Paxson LLP<br>1500 Market Street<br>Suite 3500E
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=LEGIS1021">Legislative Committee of the Probate and Trust Law Section</a>


    								<div class="popstyle" id="dekLEGIS1021">




    											<b>Time:</b>
    											<br>
    											04:00 PM

    												to
    												05:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Pepper Hamilton LLP<br>3000 Two Logan Square<br>18th and Arch Streets<br>Philadelphia, PA 19103
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=CF1021">Elections Committee</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekCF1021">




    											<b>Time:</b>
    											<br>
    											01:00 PM

    												to
    												03:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>10th Floor Board Room<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=SCL102115">CANCELED - State Civil  Lit.  "Look Good, Feel Good" Drive</a>


    								<div class="popstyle" id="dekSCL102115">




    											<b>Time:</b>
    											<br>
    											05:30 PM

    												to
    												07:30 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Del Frisco's<br>1426 Chestnut Street<br>Philadelphia, PA<p>
    									<br>

    								</p></div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=CP1021">City Policy Committee Meeting</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekCP1021">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Conference Center
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=YLD1021">YLD Cabinet</a>


    								<div class="popstyle" id="dekYLD1021">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Committee Room South<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=LEGAL1021">Legal Line</a>


    								<div class="popstyle" id="dekLEGAL1021">




    											<b>Time:</b>
    											<br>
    											05:00 PM

    												to
    												08:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>LRIS Offices<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth thu"><div class="date">22</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=BLS1022">Giants of the Business Bar</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekBLS1022">




    											<b>Time:</b>
    											<br>
    											05:30 PM

    												to
    												07:30 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Wells Fargo History Museum<br>123 South Broad Street<br>
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=ACE102215">Annual ACE Meeting and CLE Training</a>


    								<div class="popstyle" id="dekACE102215">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												03:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Pennsylvania Bar Institute<br>Wanamaker Building, 10th Floor<br>100 Penn Square East
    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth fri"><div class="date">23</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=YLBOOT1023">YLD Bootcamp</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekYLBOOT1023">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												05:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Conference Center<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=PUBLIC1023">UKNITE</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekPUBLIC1023">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												01:30 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Committee Room<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=SSD1023">Social Security Disability Benefits Committee Meeting</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekSSD1023">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>10th Floor Board Room<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=YLDHH15">YLD Networking Happy Hour</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekYLDHH15">




    											<b>Time:</b>
    											<br>
    											05:00 PM

    												to
    												07:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									The Field House<br>1150 Filbert Street<br>
    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="col7 thismonth sat"><div class="date">24</div>
    						<div class="cell">

    						</div>
    					</td>

    				</tr>

    				<tr>

    					<td width="14%" valign="top" class="col1 thismonth sun"><div class="date">25</div>
    						<div class="cell">

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth mon"><div class="date">26</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=YLDOCT26">YLD Executive Committee Meeting</a>


    								<div class="popstyle" id="dekYLDOCT26">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>10th Floor Board Room<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=ED1026">Equitable Distribution Committee Meeting</a>


    								<div class="popstyle" id="dekED1026">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Klehr Harrison Harvey Branzburg LLP<br>1835 Market Street<br>Suite 1400
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=CF1026">Chancellor's Forum</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekCF1026">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Conference Center<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth tue"><div class="date">27</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=FED1027">Federal Courts Committee Meeting</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekFED1027">




    											<b>Time:</b>
    											<br>
    											12:30 PM

    												to
    												02:30 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Conference Center<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=BP1027">Business Planning Committee of the Probate and Trust Section</a>


    								<div class="popstyle" id="dekBP1027">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Saul Ewing LLP<br>Centre Square West<br>1500 Market Street, 38th Floor<br>Philadelphia, PA 19102
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=WIP10273">Women in the Profession Committee Meeting</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekWIP10273">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>10th Floor Board Room<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=BOARDOCT29">Board of Governors</a>


    								<div class="popstyle" id="dekBOARDOCT29">




    											<b>Time:</b>
    											<br>
    											04:00 PM

    												to
    												06:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>10th Floor Board Room<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=TAXPR1027">Tax Committee of the Probate and Trust Law Section</a>


    								<div class="popstyle" id="dekTAXPR1027">




    											<b>Time:</b>
    											<br>
    											04:00 PM

    												to
    												06:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Cozen O'Connor<br>One Liberty Place<br>1650 Market Street<br>Philadelphia, PA 19103
    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth wed"><div class="date">28</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=LGBT1028">LGBT Rights Committee Meeting</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekLGBT1028">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Committee Room South<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=LFL1028">Law Firm Laboratory</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekLFL1028">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>10th Floor Board Room<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=CJSPIS1028">A Welcome Reception for New Public Interest Leaders</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekCJSPIS1028">




    											<b>Time:</b>
    											<br>
    											05:30 PM

    												to
    												07:30 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Chima<br>1901 JFK Blvd<br><br>Philadelphia, PA
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=CUS1028">Custody Committee Meeting</a>


    								<div class="popstyle" id="dekCUS1028">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Obermayer Rebmann Maxwell &amp; Hippel LLP<br>One Penn Center, 19th Floor<br>1617 JFK Blvd.<br>Philadelphia, PA 19103
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=IMMOCT28">Immigration Law Committee Meeting</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekIMMOCT28">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Conference Center<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth thu"><div class="date">29</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=BA102915">"Disgraced" A Bar Academy Event With the Phila. Theatre Co.</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekBA102915">




    											<b>Time:</b>
    											<br>
    											08:00 PM

    												to
    												10:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Suzanne Roberts Theater<br>480 S. Broad St.<br><br>Philadelphia, PA 19146
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=CG1029">Housing Working Group- Civil Gideon Task Force</a>


    								<div class="popstyle" id="dekCG1029">




    											<b>Time:</b>
    											<br>
    											08:30 AM

    												to
    												10:00 AM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>10th Floor Board Room<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=YLDOCT28">Board Observer Program CLE</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekYLDOCT28">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												01:30 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Conference Center
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=RULES1007">Rules and Procedure Committee Meeting</a>

    										<img alt="RSVP" border="0" hspace="3" src="img/reply.gif">


    								<div class="popstyle" id="dekRULES1007">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>10th Floor Board Room<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=LRIS1029">LRIS Committee</a>


    								<div class="popstyle" id="dekLRIS1029">




    											<b>Time:</b>
    											<br>
    											12:00 PM

    												to
    												02:00 PM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>11th Floor Committee Room South<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="thismonth fri"><div class="date">30</div>
    						<div class="cell">

    								<div class="title">
    									<a href="http://www.philadelphiabar.org/page/EventDetails?appNum=2&amp;eventID=SEC1030">Section and Division Chairs Meeting</a>


    								<div class="popstyle" id="dekSEC1030">




    											<b>Time:</b>
    											<br>
    											08:30 AM

    												to
    												10:30 AM




    										<br>
    										<b>Location:</b>
    										<br>

    									Philadelphia Bar Association<br>1101 Market Street<br>10th Floor Board Room<br>Philadelphia, PA 19107
    									<br>

    								</div></div>

    						</div>
    					</td>

    					<td width="14%" valign="top" class="col7 thismonth sat"><div class="date">31</div>
    						<div class="cell">

    						</div>
    					</td>

    				</tr>

    		</tbody></table>
	</center>
</form>
</div>
		</div><!-- /content area -->
	</div>

<?php cb(); ?>
