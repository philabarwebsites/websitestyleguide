<?php require('chrome.php'); ct(); ?>

    <div class="subpage-header" data-bg="img/subheader.jpg"></div>

	<div class="row content-wrap">
		<div class="col-md-3 col-sm-5 hidden-xs sidebar first-sidebar">

<div class="blue titled outline block">
    <div class="block-inner">
        <h3 class="title">Fast Facts</h3>
        <div class="content">
          <h4>Albert S. Dandridge III</h4>
          <ul>
            <li>Partner, Chief Diversity Officer and chair the Securities Practice Group at Schnader Harrison Segal &amp; Lewis LLP</li>
            <li>Bachelor's degree and J.D. from Temple University</li>
            <li>Decorated Vietnam Combat Veteran with the U.S. Marine Corps</li>
          </ul>
          <p>
          He can be reached at <a href="mailto:chancellor@philabar.org">chancellor@philabar.org</a>
          </p>
          <hr/>
          <p>
          Hear Albert S. Dandridge III outline his plans for 2015 in this <a href="#">podcast</a>.
          </p>
        </div>
    </div>
</div>
		</div> <!-- /end first sidebar -->

        <div class="col-md-13 col-sm-11 main-content">

  <h1>Policy and Leadership</h1>
  <h2>Meet Our Chancellor</h2>
  <h3>Albert S. Dandridge III</h3>
  <img src="img/chancellor.jpg" align="left" class="image left">
<p>
    Albert S. Dandridge III believes that the overriding mission of the Association is to make lawyers proud that they are lawyers. Known as being a speaker of truth and a champion of diversity and inclusion, our 88th Chancellor believes it is important for the Association to focus on community and collaboration.
</p><p>
    "We are making strides in the inclusion of all races, genders and cultures," said Dandridge. "We are reaching across the world to share positive solutions to problems and issues that affect other bar associations and the world community, not just our Bar Association and city. But, we could be doing more and we could be doing more to foster collective endeavors."
</p><p>
    In the spirit of giving back, Al will also focus on serving the legal profession as well as serving our veterans. He believes that "participating in the Bar Association supports the entire city, not just the legal community."
</p><p>
    Al is a partner at Schnader Harrison Segal &amp; Lewis LLP. He also serves as the firm's Chief Diversity Officer and chair of its Securities Practice Group. Before joining Schnader, Al served the Securities and Exchange Commission (SEC) as associate director of small business and international corporate finance in the SEC's Division of Corporation Finance. In addition, he served as special counsel in the Division of Corporation Finance.
</p><p>
    A true Philadelphian, Al earned his bachelor's degree and J.D. from Temple University and then went on to earn his LL.M. from the University of Pennsylvania Law School.
</p><p>
    Al is a decorated Vietnam Combat Veteran with the U.S. Marine Corps and was awarded, among many honors, the Bronze Star with Combat "V" for Valor and the Purple Heart.
</p><p>
    From the Philadelphia Diversity Law Group to The Legal Intelligencer to USO of Pennsylvania and Southern New Jersey, Al has given much of his time to serve on a number of law-related and community-related boards over the years, serving the citizens of Philadelphia and beyond.
</p><p>
    Some of Al's other recent distinctions include that he was this year's recipient of The Barristers' Association of Philadelphia's Honorable William F. Hall Award; last year's recipient of The Legal Intelligencer's Lifetime Achievement Award; last year's Philadelphia Business Journal Minority Business Leader Award winner; and, he was awarded the 2011 John Stephen Baerst Award for Excellence in Teaching by Morin Center at Boston University School of Law, where he served as an adjunct professor for 16 years.
</p><p>
    Additionally, Al is listed in The Best Lawyers in America for corporate law (2007-2015) and has been noted as a "Pennsylvania Super Lawyer" for securities and venture finance law (2004-2007).
</p><p>
    Al and his wife Vera reside in Chestnut Hill, where they have been active in their community.
</p>

		</div><!-- /content area -->
	</div>

<?php cb(); ?>
