<?php require('chrome.php'); ct(); ?>
    
    <div class="subpage-header" data-bg="img/subheader.jpg"></div>

	<div class="row content-wrap">
		<div class="col-md-3 hidden-sm sidebar first-sidebar">
			<div class="subnav block">
				<?php include('sidenav.php'); ?>
			</div> <!-- /subnav block -->

            <div class="plain left image blue block">
                <div class="block-inner">
                    <img src="img/thumb-top10.jpg">
                    <h3 class="small bold title">Block Title</h3>
                    <div class="content">
                        <p>Quisque facilisis erat a dui. Nam malesuada ornare dolor.</p>
                        <p><a href="#" class="pba-small grey button">Download</a></p>
                    </div>
                </div>
            </div><!-- /block -->


            <div class="green titled outline block">
                <div class="block-inner">
                    <h3 class="medium title">Renew Your Membership</h3>
                    <div class="content">
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
                        <a href="#" class="pba-small semitransparent grey button">Some link text here</a>
                    </div>
                </div>
            </div><!-- /block -->



            <div class="plain image outline titled orange block">
                <div class="block-inner">
                    <h3 class="small bold title">Block Title</h3>
                    <img src="img/thumb-top10.jpg">
                    <div class="content">
                        <p>Quisque facilisis erat a dui. Nam malesuada ornare dolor.</p>
                        <p><a href="#" class="pba-small grey button">Download</a></p>
                    </div>
                </div>
            </div><!-- /block -->

		</div> <!-- /end first sidebar -->
		
        <div class="col-md-9 col-sm-11 main-content">
<h1>Typography</h1>
<p>The following page describes many aspects of the text styles available. Use the menu on the left to jump to a section.</p>
<hr>
<a name="font"></a><h2>Fonts</h2>
<p>Base font is 'Lato' embedded via SCSS from <a href="https://www.google.com/fonts/specimen/Lato">Google Fonts</a>.  </p>
<p><strong>Details:</strong>  </p>
<p>Base Font Size: 16px;  </p>
<p>Default text color: #333; </p>
<p><a name="head"></a></p>
<h2>Headings</h2>
<p>Header tags should be used to introduce a paragraph and break up the text on a page. </p>
<h1>This is a Heading 1 Tag</h1>
<h2>This is a Heading 2 Tag</h2>
<h3>This is a Heading 3 Tag</h3>
<h4>This is a Heading 4 Tag</h4>
<h5>This is a Heading 5 Tag</h5>
<h6>This is a Heading 6 Tag</h6>
<hr>
<a name="para"></a><h2>Paragraphs</h2>
<p>Size: 16px;</p>
<p>Line-height: 1.42</p>
<p>Color: #333;</p>
<p>Alignment: Justified</p>
<p>&nbsp;</p>
<hr>
<a name="list"></a><h2>Lists</h2>
<h3>Ordered List</h3>
<ol>
  <li>Aliquam adipiscing libero vitae leo</li>
  <li>Donec id eros eget quam aliquam gravida</li>
  <li>Donec id eros eget quam aliquam gravida</li>
  <li>Curabitur quis dui sit amet elit luctus aliquam</li>
  <li>Donec non tortor in arcu mollis feugiat</li>
</ol>

<h3>Unordered List</h3>
<ul>
  <li>Aliquam adipiscing libero vitae leo</li>
  <li>Donec id eros eget quam aliquam gravida</li>
  <li>Donec id eros eget quam aliquam gravida</li>
  <li>Curabitur quis dui sit amet elit luctus aliquam</li>
  <li>Donec non tortor in arcu mollis feugiat</li>
</ul>

<h3>Square Bullets</h3>
<div class="note">Add class "square" to the ul element.</div>
<ul class="square blue">
  <li>Aliquam adipiscing libero vitae leo</li>
  <li>Donec id eros eget quam aliquam gravida</li>
  <li>Donec id eros eget quam aliquam gravida</li>
  <li>Curabitur quis dui sit amet elit luctus aliquam</li>
  <li>Donec non tortor in arcu mollis feugiat</li>
</ul>

<h3>Checkmark Bullets</h3>
<div class="note">Add class "checkmark" to the ul element.</div>
<ul class="checkmark blue">
  <li>Aliquam adipiscing libero vitae leo</li>
  <li>Donec id eros eget quam aliquam gravida</li>
  <li>Donec id eros eget quam aliquam gravida</li>
  <li>Curabitur quis dui sit amet elit luctus aliquam</li>
  <li>Donec non tortor in arcu mollis feugiat</li>
</ul>



<hr>
<a name="quot"></a><h2>Blockquotes</h2>
<div class="note">Blockquotes are used to call out text in certain circumstances. Below are the types of blockquotes available.</div>

<h3>Default</h3>
<blockquote>
    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer.
</blockquote>

<h3>Left Aligned</h3>
<blockquote class="left">
    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer.
</blockquote>

<h3>Right Aligned</h3>
<blockquote class="right">
    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer.
</blockquote>

<h3>Color</h3>
<div class="note">Add a color class to the element: blue, orange, green. The absence of a color class will render a grey color treatment as seen in the default blockquote version.</div class="note">
<blockquote class="left blue">
    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer.
</blockquote>
<blockquote class="right orange">
    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer.
</blockquote>
<blockquote class="green">
    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer.
</blockquote>


<hr>
<a name="foot"></a><h2>Footnotes</h2>
<div class="note">Footnotes are added by adding a class "footnote" to the parent element. In cases where you'd like multiple footnotes, its best to use a ul or ol object and add the class there. Otherwise adding this class to any wrapper (p, div) will render a footnote as 12px font size, a top border, top margin and padding.</div>
<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer.</p>
<p class="footnote">Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi.</p>
<br><br><br>

<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer.</p>
<div class="footnote">
    <ul>
        <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
        <li>Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. </li>
        <li>Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. </li>
        <li>Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer.</li>
    </ul>
</div>
<hr>
<a name="tabl"></a><h2>Tables</h2>
<div class="note">
<ul>
<li><strong>Striped</strong>: Add class "striped" to the table element to make a zebra pattern on the rows. </li>
<li><strong>Bordered</strong>: Add class "bordered" to the table element to make a border around the table element. </li>
<li><strong>Font-Size</strong>: Add class: small, medium or large to the table element. </li>
<li><strong>Color</strong>: Add color classes to the table element to control the color of the table. Choose from blue, orange or green. The default color is grey. </li>
</ul>
</div>
<h3>Striped</h3>
<div class="note">Add class: striped</div>
<table class="striped">
    <thead>
        <tr> <th>Header</th> <th>Header</th> <th>Header</th> <th>Header</th> <th>Header</th> </tr>
    </thead>
    <tbody>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
    </tbody>
</table>

<h3>Bordered</h3>
<div class="note">Add class: bordered</div>
<table class="striped bordered">
    <thead>
        <tr> <th>Header</th> <th>Header</th> <th>Header</th> <th>Header</th> <th>Header</th> </tr>
    </thead>
    <tbody>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
    </tbody>
</table>

<h3>Small</h3>
<div class="note">Add class: small (renders text at 14px)</div>
<table class="small striped bordered">
    <thead>
        <tr> <th>Header</th> <th>Header</th> <th>Header</th> <th>Header</th> <th>Header</th> </tr>
    </thead>
    <tbody>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
    </tbody>
</table>


<h3>Large</h3>
<div class="note">Add class: large (renders text at 18px)</div>
<table class="large striped bordered">
    <thead>
        <tr> <th>Header</th> <th>Header</th> <th>Header</th> <th>Header</th> <th>Header</th> </tr>
    </thead>
    <tbody>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
    </tbody>
</table>

<h3>Colors</h3>
<div class="note">Omit a color class to render a default grey table.</div>
<table class="striped bordered">
    <thead>
        <tr> <th>Header</th> <th>Header</th> <th>Header</th> <th>Header</th> <th>Header</th> </tr>
    </thead>
    <tbody>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
    </tbody>
</table>
<hr>
<div class="note">Add class: blue</div>
<table class="blue striped bordered">
    <thead>
        <tr> <th>Header</th> <th>Header</th> <th>Header</th> <th>Header</th> <th>Header</th> </tr>
    </thead>
    <tbody>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
    </tbody>
</table>
<hr>
<div class="note">Add class: orange</div>
<table class="orange striped bordered">
    <thead>
        <tr> <th>Header</th> <th>Header</th> <th>Header</th> <th>Header</th> <th>Header</th> </tr>
    </thead>
    <tbody>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
    </tbody>
</table>
<hr>
<div class="note">Add class: green</div>
<table class="green striped bordered">
    <thead>
        <tr> <th>Header</th> <th>Header</th> <th>Header</th> <th>Header</th> <th>Header</th> </tr>
    </thead>
    <tbody>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
        <tr> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> <td>Some Data</td> </tr>
    </tbody>
</table>


<table border="0" cellspacing="0" cellpadding="0" class="striped orange directory">
  <tbody><tr>
    <td>Name</td>
    <td>Title</td>
    <td>Phone</td>
    <td>E-Mail</td>
  </tr>

  <tr>
    <td>Archer, Claudia</td>
    <td>(Caterer)</td>
    <td>6306</td>
    <td><a href="mailto:carcher@philabar.org">carcher@philabar.org</a></td>
  </tr>

  <tr>
    <td>Avakian-Hardaway, Meredith Z.</td>
    <td>(Director of Communications and Marketing)</td>
    <td>6342</td>
    <td><a href="mailto:mavakianhardaway@philabar.org">mavakianhardaway@philabar.org</a></td>
  </tr>

  <tr>
    <td>Bonfiglio, Mary Jane</td>
    <td>(Administrative Support, Public and Legal Services)</td>
    <td>6319</td>
    <td><a href="mailto:mbonfiglio@philabar.org">mbonfiglio@philabar.org</a></td>
  </tr>

  <tr>
    <td>Coleman, Lorraine</td>
    <td>(Administrative Assistant)</td>
    <td>6309</td>
    <td><a href="mailto:lcoleman@philabar.org">lcoleman@philabar.org</a></td>
  </tr>

  <tr>
    <td>DeLaurentis-Rockey, Diana</td>
    <td>(Accounts Payable Coordinator)</td>
    <td>6316</td>
    <td><a href="mailto:ddelaurentis@philabar.org">ddelaurentis@philabar.org</a></td>
  </tr>

  <tr>
    <td>Driscoll, Cecelia</td>
    <td>(Administrative Assistant, Philadelphia Bar Foundation)</td>
    <td>6337</td>
    <td><a href="mailto:cdriscoll@philabar.org">cdriscoll@philabar.org</a></td>
  </tr>

  <tr>
    <td>Greenspan, Barry</td>
    <td>(Chief Technology Officer)</td>
    <td>6327</td>
    <td><a href="mailto:bgreenspan@philabar.org">bgreenspan@philabar.org</a></td>
  </tr>

  <tr>
    <td>Hilburn-Holmes, Jessica</td>
    <td>(Executive Director, Philadelphia Bar Foundation)</td>
    <td>6347</td>
    <td><a href="mailto:jhilburnholmes@philabar.org">jhilburnholmes@philabar.org</a></td>
  </tr>

  <tr>
    <td>Kazaras, Paul</td>
    <td>(Assistant Executive Director)</td>
    <td>6328</td>
    <td><a href="mailto:pkazaras@philabar.org">pkazaras@philabar.org</a></td>
  </tr>

  <tr>
    <td>Klitsch, Charlie</td>
    <td>(Director of Public and Legal Services)</td>
    <td>6326</td>
    <td><a href="mailto:cklitsch@philabar.org">cklitsch@philabar.org</a></td>
  </tr>

  <tr>
    <td>Knight, Susan</td>
    <td>(Chief Financial Officer/Director of Administration)</td>
    <td>6325</td>
    <td><a href="mailto:sknight@philabar.org">sknight@philabar.org</a></td>
  </tr>

  <tr>
    <td>McCloskey, Tracey</td>
    <td>(Director of Meetings &amp; Special Events)</td>
    <td>6360</td>
    <td><a href="mailto:tmccloskey@philabar.org">tmccloskey@philabar.org</a></td>
  </tr>

  <tr>
    <td>Morris-Tracey, Andrea</td>
    <td>(Manager of Member Services)</td>
    <td>6313</td>
    <td><a href="mailto:amorris@philabar.org">amorris@philabar.org</a></td>
  </tr>

  <tr>
    <td>Petit, Dawn</td>
    <td>(Meetings Coordinator)</td>
    <td>6367</td>
    <td><a href="mailto:dpetit@philabar.org">dpetit@philabar.org</a></td>
  </tr>

  <tr>
    <td>Rogers, Tom</td>
    <td>(Managing Editor, Publications)</td>
    <td>6345</td>
    <td><a href="mailto:trogers@philabar.org">trogers@philabar.org</a></td>
  </tr>

  <tr>
    <td>Seefeld, Amy</td>
    <td>(Senior Staff Counsel, Public &amp; Legal Services)</td>
    <td>6369</td>
    <td><a href="mailto:aseefeld@philabar.org">aseefeld@philabar.org</a></td>
  </tr>

  <tr>
    <td>Stegossi, Michael</td>
    <td>(Staff Accountant)</td>
    <td>6315</td>
    <td><a href="mailto:mstegossi@philabar.org">mstegossi@philabar.org</a></td>
  </tr>

  <tr>
    <td>Tarasiewicz, Mark</td>
    <td>(Executive Director)</td>
    <td>6346</td>
    <td><a href="mailto:mtarasiewicz@philabar.org">mtarasiewicz@philabar.org</a></td>
  </tr>

  <tr>
    <td>Terry, Wesley</td>
    <td>(Web Manager)</td>
    <td>6339</td>
    <td><a href="mailto:wterry@philabar.org">wterry@philabar.org</a></td>
  </tr>

  <tr>
    <td>White, Florence</td>
    <td>(Administrative Support, Member Services)</td>
    <td>6357</td>
    <td><a href="mailto:fwhite@philabar.org">fwhite@philabar.org</a></td>
  </tr>

  <tr>
    <td>Zebe, Merril</td>
    <td>(Public Interest Coordinator)</td>
    <td>6355</td>
    <td><a href="mailto:mzebe@philabar.org">mzebe@philabar.org</a></td>
  </tr>

</tbody></table>

		</div><!-- /content area -->
		<div class="col-md-4 col-sm-5 sidebar second-sidebar">
			<?php include('sidebar-right.php'); ?>

		</div><!-- / sidebar-second -->
	</div>

<?php cb(); ?>