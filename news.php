<?php require('chrome.php'); ct(); ?>

    <div class="subpage-header" data-bg="img/subheader.jpg"></div>

	<div class="row content-wrap">
		<div class="col-md-3 hidden-sm sidebar first-sidebar">
			<?php include("sidenews.php"); ?>
		</div> <!-- /end first sidebar -->

        <div class="col-md-13 col-sm-11 main-content">

    <h1>News</h1>
<div id="widecentercontent">
  <h2>Chancellor's Forum: Kiss, Bow or Shake Hands, with Terri Morrison Oct. 26</h2>
  <img src="img/TerriMorrison.jpg" alt="speaker photo" class="left aligned" vspace="4"/>
  <p>
  The Business Law Section and the Diversity in the Profession Committee are co-presenting a Chancellor's forum titled "Kiss, Bow or Shake Hands: Communicating Across Cultures for Lawyers" at the Philadelphia Bar Association, 11th Floor Conference Center, on Monday, Oct. 26, at 12 p.m. Terri Morrison, co-author of the bestselling book series "Kiss, Bow or Shake Hands" will deliver an entertaining and informative presentation on intercultural business and legal practices, protocol and marketing.
</p><p>
  Morrison will help you acquire valuable cultural intelligence in global communication styles - from direct speech with Swiss bankers to silence with South Koreans. Understanding international decision-making priorities, negotiating styles and motivating factors can give you a new perspective on attracting and retaining global clients.
</p><p>
  The Philadelphia Bar Association is located at 1101 Market St. There is no cost to attend this program, but registration is required. Lunch is available for purchase to those who register in advance. The cost of lunch is $9 for members of the Philadelphia Bar Association and $12 for non-members. For more information and to register, <a href="#">click here</a>.
</p>



    </div>

		</div><!-- /content area -->

	</div>

<?php cb(); ?>
