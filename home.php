<?php require('chrome.php'); ct(); ?>

	<div class="hero">
	<div class="hero-inner">
		<ul class="nobullet cycle-slideshow"
    data-cycle-fx="fade"
    data-cycle-pause-on-hover="true"
    data-cycle-swipe="true"
    data-cycle-speed="600"
    data-cycle-slides="> li"
    data-cycle-auto-height="1300:388">
			<li data-slide-bg="slide-1-bg.jpg"><div class="img"><img src="img/slide-1.jpg"></div><div class="caption">This is a caption</div></li>
			<li data-slide-bg="slide-2-bg.jpg"><div class="img"><img src="img/slide-2.jpg"></div></li>
			<li data-slide-bg="slide-3-bg.jpg"><div class="img"><img src="img/slide-3.jpg"></div></li>
		</ul>
		</div>
	</div><!-- /end hero -->

	<div class="row content-wrap">
		<div class="col-md-6 col-sm-5 orange bordered block">
			<div class="block-inner">
				<h3 class="title">Upcoming Events</h3>
				<div class="content">
					<ul class="blue square bullet strong links list">
						<li><a href="http://benchbar.philadelphiabar.org">Bench-Bar &amp; Annual Conference - Oct. 16-17</a></li>
						<li><a href="https://www.philadelphiabar.org/page/EventDetails&eventID=CF1026">Chancellor's Forum - Oct. 26</a></li>
						<li><a href="https://www.philadelphiabar.org/page/EventDetails&eventID=BA102915">"Disgraced" Bar Academy Event - Oct. 29</a></li>
						<li><a href="https://www.philadelphiabar.org/page/EventDetails&eventID=CHAN1110">Chancellor's Forum - Hot Issues Facing Global Companies - Nov. 5</a></li>
						<li><a href="https://www.philadelphiabar.org/page/EventDetails&eventID=CF111015">Chancellor's Forum - Nov. 10</a></li>
					</ul>
					<p class="centered"><a href="calendar.php" class="pba-small grey right arrow button">View Calendar</a></p>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-5 orange block home-for-lists">
			<div class="block-inner">
				<h3 class="linked right arrow title"><a href="#">For Members</a></h3>
				<div class="content">
					<ul class="blue strong links nobullet list">
						<li><a href="#">Join/Renew</a></li>
						<li><a href="#">Membership Brochure</a></li>
						<li><a href="#">Sections and Committees</a></li>
						<li><a href="#">Diversity and Inclusion Action Plan</a></li>
					</ul>
				</div>
				<h3 class="linked right arrow title"><a href="#">For The Public</a></h3>
				<div class="content">
					<ul class="blue strong links nobullet list">
						<li><a href="#">Online Lawyer Referral</a></li>
						<li><a href="#">Fee Disputes</a></li>
						<li><a href="#">Community Outreach</a></li>
						<li><a href="#">Speakers Bureau</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-sm-6 orange block tight">
			<div class="block-inner">
				<h3 class="title">Meet Our Chancellor</h3>
				<div class="content">
					<h3>Albert S. Dandridge III</h3>
					<img src="img/chancellor.jpg" align="left" class="image left">
					<p>Albert S. Dandridge III believes that the overriding mission of the Association is to make lawyers proud that they are lawyers. Known as being a speaker of truth and a champion of diversity and inclusion, our 88th Chancellor believes it is important for the Association to focus on community and collaboration.</p>
					<p><a href="chancellor.php" class="pba-small grey right arrow button">Read full bio</a></p>
				</div>
			</div>
		</div>
	</div><!-- row top three blocks -->

	<div class="light grey divider">&nbsp;</div>

	<div class="row content-wrap">
		<div class="col-md-10 col-sm-8 orange block">
			<h3 class="title">Latest News</h3>
			<ul class="large content nobullet tight strong news links list">
				<li>
				<a href="news.php">Chancellor's Forum: Kiss, Bow or Shake Hands, with Terri Morrison Oct. 26</a>
				<img src="img/TerriMorrison.jpg" alt="speaker photo" width="65" height="70" class="left aligned" vspace="4"/>
				<p>
			 	The Business Law Section and the Diversity in the Profession Committee are co-presenting a Chancellor's forum titled "Kiss, Bow or Shake Hands: Communicating Across Cultures for Lawyers" at the Philadelphia Bar Association, 11th Floor Conference Center.
				</p>
				</li>

				<li>
				<a href="#">Philadelphia Bar Association Announces Judicial Ratings, Releases Poll Results</a>
				<p>
				Albert S. Dandridge III, Chancellor of the Philadelphia Bar Association, today announced the results of the Association's investigation into the qualifications of candidates for the Philadelphia Court of Common Pleas and Municipal Court in the Nov. 3 general election. The investigation was conducted by the Association's Commission on Judicial Selection and Retention.
				</p>
				</li>


				<li>
				<a href="#">RESCHEDULED: Heather Jarvis Program</a>
				<p>
				The Student Loan Repayment Options and Public Service Loan Forgiveness program featuring Heather Jarvis has been RESCHEDULED and will be held tomorrow, October 6 at 3 p.m. in the 11th Floor Conference Center of the Philadelphia Bar Association. Inclement weather in North Carolina has prevented Ms. Jarvis from arriving in Philadelphia today.
				</p>
				</li>

				<li>
				<a href="#">Bar Academy: "Disgraced" at the Philadelphia Theatre Company Oct. 29</a>
				<img src="img/Disgraced_credits.jpg" align="left" width="113" height="70" alt="Disgraced Credits" class="left aligned" vspace="4"/>
				<p>
				 Join the Philadelphia Bar Academy, in partnership with the Philadelphia Theatre Company, and enjoy a performance of the play "Disgraced," winner of the 2013 Pulitzer Prize, at the Suzanne Roberts Theater, on Thursday, Oct. 29, at 8 p.m. "Disgraced" portrays Amir Kapoor, a successful Pakistani-American lawyer, living in New York and competing for a partnership, who is hiding his upbringing from colleagues.
				</p>
				</li>

				<li>
				<a href="#">Climate Change and National Security Oct. 14</a>
				<img src="img/climatechangeposter.jpg" alt="article image" width="150" height="70" class="left aligned" vspace="4"/>
				<p>
				You are invited to attend a complimentary lunch and discussion titled "Climate Change and National Security: People, not Polar Bears" with Rear Adm. David W. Titley (ret.), Ph.D, on Wednesday, Oct. 14, at Temple University Beasley School of Law, Klein K2B, at 12 p.m. Rear Adm. Titley is a professor of practice in meteorology and founding director of the Center for Solutions to Weather and Climate Risk at the Pennsylvania State University Department of Meteorology.
				</p>
				</li>

				<li>
				<a href="#">Philadelphia Bar Association Celebrates National Pro Bono Week</a>
				<p>
				Members of the Philadelphia legal community are invited to participate in the following pro bono recruitment, recognition and training events that will be held during the month of October 2015 to commemorate the American Bar Association's National Pro Bono Celebration, a coordinated national effort held during the week of Oct. 25-31 to encourage and support local efforts to expand the delivery of pro bono legal services.
				</p>
				</li>


				<li>
				<a href="#">Deadline Oct. 13: Nominations Sought for the PNC Achievement Award</a>
				<p>
				Nominations are being accepted for the 2015 PNC Achievement Award. Nominations should be made in writing. Please include a detailed statement setting forth information and reasons why you are recommending the nominee for this award.
				</p>
				</li>

				<li>
				<a href="#">Deadline Oct. 13: Nominations Sought for Brennan Award</a>
				<p>
				Nominations are being accepted for the Philadelphia Bar Association's prestigious Justice William J. Brennan Jr. Distinguished Jurist Award. The award will be presented at the Association's Annual Meeting on Tuesday, Dec. 8.
				</p>
				</li>

				<li>
				<a href="#">Giants of the Business Bar Oct. 22</a>
				<p>
				The Business Law Section is recognizing Amy Boss, trustee professor, Drexel University Thomas R. Kline School of Law, on Thursday, Oct. 22, at the Wells Fargo History Museum, at 5:30 p.m. Professor Boss is one of the nation's leading commercial law scholars. | read more
				</p>
				</li>

				<li>
				<a href="#">Message From the Chancellor: Clemency Project 2014 Seeks Volunteers</a>
				<p>
				As Philadelphia lawyers, the Philadelphia Bar Association asks for your assistance with Clemency Project 2014. Clemency Project 2014 is a working group made up of the American Bar Association, The American Civil Liberties Union, Families Against Mandatory Minimums, Federal Public and Community Defenders and the National Association of Criminal Defense Lawyers.
				</p>
				</li>


	</ul>
		</div>
		<div class="col-md-6 col-sm-8 orange twitter block">
			<h3 class="title">Twitter Feed</h3>
			<img src="img/twitterfeed.jpg">
			<!-- <a class="twitter-timeline" href="https://twitter.com/PhilaBar" data-widget-id="622939351161405441">Tweets by @PhilaBar</a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script> -->
		</div>
	</div><!-- /row news/twitter -->


	<div class="row content-wrap">
		<div class="col-md-16 blue bordered publications block">
		<h3 class="orange title">Philadelphia Bar Association Publications</h3>
			<ul class="large content left thumb nobullet tight strong news links list three across">
				<li>
					<a href="#">Upon Further Review</a>
					<img src="img/pub-thumb-upon.jpg">
					<p>A new web publication that links lawyers to local legal news and analysis.</p>
				</li>
				<li>
					<a href="#">Philadelphia Bar Reporter</a>
					<img src="img/pub-thumb-bar-reporter.jpg">
					<p>The Association's monthly newspaper. It focuses on Association activities, member participation and services.</p>
				</li>
				<li>
					<a href="#">The Philadelphia Lawyer</a>
					<img src="img/pub-thumb-phila-lawyer.jpg">
					<p>The Association's quarterly magazine devoted to the law, lifestyles and other topics of interest to Philadelphia lawyers.</p>
				</li>
				<li>
					<a href="#">The Legal Directory</a>
					<img src="img/pub-thumb-legal-dir.jpg">
					<p>A list of the attorneys and firms in the five-county Philadelphia area.</p>
				</li>
				<li>
					<a href="#">Bar Reporter Online </a>
					<img src="img/pub-thumb-bro.jpg">
					<p>The Association's bi-weekly e-newsbrief. E-mailed to Association members every Monday and Thursday mornings.</p>
				</li>
				<li>
					<a href="#">Young Lawyers Division EZine</a>
					<img src="img/pub-thumb-young.jpg">
					<p>News of the Philadelphia Bar Association's Young Lawyers Division</p>
				</li>
			</ul>
		</div>
	</div> <!-- /row publications -->


	<div class="row content-wrap">
		<div class="col-md-16 orange block">
		<!-- <h3 class="title">Sponsors of the Philadelphia Bar Association</h3> -->
			<ul class="five across nobullet sponsor list">
				<li><a href="#"><img src="img/sponsors/USI_Affinity_logo_287.jpg"></a></li>
				<li><a href="#"><img src="img/sponsors/lawpaylogo.jpg"></a></li>
				<li><a href="#"><img src="img/sponsors/PBI-50.jpg"></a></li>
				<li><a href="#"><img src="img/sponsors/PNC_WeMgmt_4C.png"></a></li>
				<li><a href="#"><img src="img/sponsors/Veritext_Logo_Color2.jpg"></a></li>
			</ul>
		</div>
	</div> <!-- /row sponsors -->

<?php cb(); ?>
