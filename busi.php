<?php require('chrome.php'); ct(); ?>
    
    <div class="subpage-header" data-bg="img/subheader.jpg"></div>

	<div class="row content-wrap">
		<div class="col-md-3 hidden-sm sidebar first-sidebar">
			<?php //include("sidebar-left.php"); ?>

			
<div class="subnav block">
	<ul class="large stackednav navigation nobullet strong link list">
	
		<li id="section0">
			
				<a class="selected" title="Business Law Section" href="http://philabar.org/page/BusinessLawSection?appNum=2">Business Law Section</a>
			
			
			
				<ul>
	
		<li id="section01">
			
				<a title="Bylaws" href="http://philabar.org/page/BusinessLawSectionBylaws?appNum=2">Bylaws</a>
			
			
			
		</li>
	
		<li id="101007100954792">
			
				<a title="Diversity Action Plan" href="http://philabar.org/WebObjects/PBAReadOnly.woa/Contents/WebServerResources/CMSResources/Diversity_Action_Plan,_Business_Law_Section.doc.pdf">Diversity Action Plan</a>
			
			
			
		</li>
	
		<li id="section02">
			
				<a title="Executive Committee" href="http://philabar.org/page/BusinessLawSectionExecutiveCommittee?appNum=2">Executive Committee</a>
			
			
			
		</li>
	
		<li id="101109161944825">
			
				<a title="Advisory Board Charter" href="http://philabar.org/WebObjects/PBAReadOnly.woa/Contents/WebServerResources/CMSResources/AdvisoryBoardCharter.pdf">Advisory Board Charter</a>
			
			
			
		</li>
	
		<li id="100514164414119">
			
				<a title="Law School Outreach" href="http://philabar.org/page/BusinessLawSectionLawSchoolOutreach?appNum=2">Law School Outreach</a>
			
			
			
		</li>
	
		<li id="section04">
			
				<a title="Newsletters" href="http://philabar.org/page/BusinessLawSectionNewsletters?appNum=2">Newsletters</a>
			
			
			
		</li>
	
		<li id="140305105509366">
			
				<a title="News Archive" href="http://philabar.org/page/BusinessLawSectionNewsArchive?appNum=2">News Archive</a>
			
			
			
		</li>
	
		<li id="section06">
			
				<a title="Event Calendar" href="http://philabar.org/page/BusinessLawSectionEventCalendar?appNum=2">Event Calendar</a>
			
			
			
		</li>
	
		<li id="131121141320526">
			
				<a title="Albert S. Dandridge III Diversity Award" href="http://philabar.org/page/BLSDiversity?appNum=2">Albert S. Dandridge III Diversity Award</a>
			
			
			
		</li>
	
		<li id="100201161648695">
			
				<a title="Dennis H. Replansky Memorial Award" href="http://philabar.org/page/BLSReplansky?appNum=2">Dennis H. Replansky Memorial Award</a>
			
			
			
		</li>
	
		<li id="100205161132668">
			
				<a title="Business Law Section Annual Awards" href="http://philabar.org/page/BLSAwards?appNum=2">Business Law Section Annual Awards</a>
			
			
			
		</li>
	
		<li id="100205161211563">
			
				<a title="Business Law Section Chairs" href="http://philabar.org/page/BLSChairs?appNum=2">Business Law Section Chairs</a>
			
			
			
		</li>
	
		<li id="050718092022363">
			
				<a title="Comment Letters" href="http://philabar.org/page/BusinessLawSectionComments?appNum=2">Comment Letters</a>
			
			
			
		</li>
	
		<li id="section07">
			
				<a title="Resources" href="http://philabar.org/page/BusinessLawSectionResources?appNum=2">Resources</a>
			
			
			
		</li>
	
		<li id="101130104315929">
			
				<a title="Section Reports" href="http://philabar.org/page/BusinessLawSectionReports?appNum=2">Section Reports</a>
			
			
			
		</li>
	
		<li id="section08">
			
				<a title="Join" href="http://philabar.org/WebObjects/PBAReadOnly.woa/Contents/WebServerResources/CMSResources/BusinessLaw12.pdf">Join</a>
			
			
			
		</li>
	
</ul>

			
		</li>
	
		<li id="section1">
			
				<a title="Criminal Justice Section" href="http://philabar.org/page/CriminalJusticeSection?appNum=2">Criminal Justice Section</a>
			
			
			
		</li>
	
		<li id="section2">
			
				<a title="Family Law Section" href="http://philabar.org/page/FamilyLawSection?appNum=2">Family Law Section</a>
			
			
			
		</li>
	
		<li id="section3">
			
				<a title="Probate and Trust Law Section" href="http://philabar.org/page/ProbateAndTrustLawSection?appNum=2">Probate and Trust Law Section</a>
			
			
			
		</li>
	
		<li id="section4">
			
				<a title="Public Interest Section" href="http://philabar.org/page/PublicInterestSection?appNum=2">Public Interest Section</a>
			
			
			
		</li>
	
		<li id="section5">
			
				<a title="Real Property Section" href="http://philabar.org/page/RealPropertySection?appNum=2">Real Property Section</a>
			
			
			
		</li>
	
		<li id="section6">
			
				<a title="State Civil Litigation Section" href="http://philabar.org/page/StateCivilLitigationSection?appNum=2">State Civil Litigation Section</a>
			
			
			
		</li>
	
		<li id="section7">
			
				<a title="Tax Section" href="http://philabar.org/page/TaxSection?appNum=2">Tax Section</a>
			
			
			
		</li>
	
		<li id="section8">
			
				<a title="Workers' Compensation Section" href="http://philabar.org/page/WorkersCompensationSection?appNum=2">Workers' Compensation Section</a>
			
			
			
		</li>
	
		<li id="section9">
			
				<a title="Young Lawyers Division" href="http://philabar.org/page/YoungLawyersDivision?appNum=2">Young Lawyers Division</a>
			
			
			
		</li>
	
</ul>

</div>



		</div> <!-- /end first sidebar -->
		
        <div class="col-md-9 col-sm-11 main-content">

<div class="defaultText" id="140707155106801">
	<h1>PHILADELPHIA BAR ASSOCIATION RESOLUTION SUPPORTING ACTION ON CLIMATE CHANGE</h1> 
	<p>
	 <strong>WHEREAS</strong>, in the last quarter century a broad scientific consensus has emerged that human activities, primarily the burning of fossil fuels, have caused an enhanced greenhouse effect that is driving great changes in the global climate including warming and increasingly extreme weather patterns that endanger humanity and will present enormous challenges for humankind in the near future<a href="#1"><sup>1</sup></a>; and </p> <p>
	 <strong>WHEREAS</strong>, the world's leading scientific organizations have warned repeatedly of the severe human and economic consequences in the near future from global warming (a/k/a global climate change) caused by greenhouse gas emissions including threats to food production, freshwater supplies, coastal infrastructure, and especially the welfare of the huge population currently living in low lying areas; and </p> <p>
	 <strong>WHEREAS</strong>, fossil fuel extraction and burning is the greatest driver of greenhouse gas emissions, and decreasing the use of fossil fuels will consequently be crucial to mitigating climate change; and </p> <p>
	 <strong>WHEREAS</strong>, our nation's first principle is that all people are created equal and enjoy "certain unalienable rights" to life, liberty, and the pursuit of happiness, and that government exists to secure these rights; and </p> <p>
	 <strong>WHEREAS</strong>, the reports from the scientific community indisputably demonstrate a serious threat to the unalienable rights to life, liberty, and the pursuit of happiness of all people now living and to be born in the future from global climate change; and </p> <p>
	 <strong>WHEREAS</strong>, the scientific community has warned that, while it is not too late to take effective action, the time left to mitigate the harm is disappearing as significant harm is now already occurring; and </p> <p>
	 <strong>WHEREAS</strong>, economically and technologically feasible means exist to protect humankind from the worst of climate change without harming the world economy; and </p> <p>
	 <strong>WHEREAS</strong>, these economically and technologically feasible means include carbon pricing policies that will capture the external costs of burning fossil fuels and certain land use patterns while creating incentives for the development and mass production of alternative energy technologies; and </p> <p>
	 <strong>WHEREAS</strong>, numerous leading economists support carbon pricing policies as a feasible and efficient means to reduce greenhouse gas emissions and prevent further harm from global climate change; and </p> <p>
	 <strong>WHEREAS</strong>, the mandate to move forward with climate mitigation policies has broad backing in society at large; and </p> <p>
	 <strong>WHEREAS</strong>, the Pennsylvania Constitution, in the Environmental Rights Amendment, Article 1, Section 27, guarantees to "all of the people, including generations yet to come," "a right to clean air, pure water, and to the preservation of the natural, scenic, historic and esthetic values of the environment;" and </p> <p>
	 <strong>WHEREAS</strong>, the problem of global warming threatens fundamental human rights, the protection of which is a subject of paramount importance to lawyers and the legal community; and </p> <p>
	 <strong>WHEREAS</strong>, lawyers can and should contribute to solutions to the problem of global warming through legal measures, including measures to substantially reduce greenhouse gas emissions; and </p> <p>
	 <strong>WHEREAS</strong>, the Philadelphia Bar Association, the oldest association of lawyers in the United States, has a history of leadership on environmental protection issues dating at least to the establishment of an Environmental Quality Committee in the early 1970s and continuing with advocacy for legislation protecting the environment and in 2010 the adoption of the Green Ribbon Sustainability Initiative to encourage environmentally friendly behaviors and practices for law firms, legal offices and individual lawyers; and </p> <p>
	 <strong>WHEREAS</strong>, the Philadelphia Bar Association acknowledges its responsibilities to continue to be a leader on environmental protection issues for the purpose of helping to shape the law for the benefit of all citizens, and especially for our children and future generations. </p> <p>
	 <strong>NOW THEREFORE, BE IT RESOLVED</strong>, that the Philadelphia Bar Association calls on the U.S. Congress, the Pennsylvania General Assembly and local governments to take immediate and effective action to protect the unalienable rights of all people to a secure and safe environment for life by:  </p> <ol type="1"> 	<li>acknowledging the reality and threat of global climate change caused by human activities as reported by the scientific community,</li> 	<li>taking immediate and effective action to substantially reduce greenhouse gas emissions, and</li> 	<li>taking other reasonable measures to focus government at the international, national, state, and local levels on the problem of climate change;</li> </ol> <p>
		 <strong>AND BE IT FURTHER RESOLVED</strong>, that the Philadelphia Bar Association calls on all people and all sectors of society, including all other associations of legal professionals, all lawyers, and all members of society, to use the powers at their disposal to address global climate change and specifically to promote policies, such as carbon pricing, to reduce the use of fossil fuels and greenhouse gas emissions. </p> <p>
	 <strong>PHILADELPHIA BAR ASSOCIATION<br> BOARD OF GOVERNORS<br> ADOPTED: June 26, 2014</strong> </p> <p>
	 <small> <a name="1"><sup>1</sup></a>See, e.g., Third National Climate Assessment, published by the U.S. Global Change Research Program (May 2014) (<a href="http://nca2014.globalchange.gov">http://nca2014.globalchange.gov</a>); <em>Fifth Assessment Report</em> of the Intergovernmental Panel on Climate Change (April 2014) (<a href="http://www.ipcc.ch/report/ar5/">http://www.ipcc.ch/report/ar5/</a>); <em>What We Know</em>, published by the American Association for the Advancement of Science (March 2014) (<a href="http://whatweknow.aaas.org">http://whatweknow.aaas.org</a>); <em>Climate Change – Evidence and Causes – An Overview from the Royal Society and the US National Academy of Sciences</em> (February 2014) (<a href="http://dels.nas.edu/resources/static-assets/exec-office-other/climate-change-full.pdf">http://dels.nas.edu/resources/static-assets/exec-office-other/climate-change-full.pdf</a>).	 </small></p>	
</div>						

		</div><!-- /content area -->
		<div class="col-md-4 col-sm-5 sidebar second-sidebar">
							
<div class="centered image block defaultext" id="060511115606366">
<div class="block-inner">
<a name="060511115606366"></a>
<a href="http://philabar.org/page/Podcast?appNum=2"><img src="http://philabar.org/WebObjects/PBAReadOnly.woa/Contents/WebServerResources/CMSResources/PodCast2.gif" width="138" height="138"></a>
</div>
</div>

<div class="blue titled outline centered image block" id="120109153349977">
<div class="block-inner">
<a name="120109153349977"></a>
<h3 class="title">Sponsors</h3>
<hr> <a href="http://thomsonreuters.com"><img src="http://philabar.org/WebObjects/PBA.woa/Contents/WebServerResources/CMSResources/thomsomreuters_logo.png" width="190" height="42" alt="Thomson Reuters" border="0"></a>	 <hr> <a href="http://mcgladrey.com/content/mcgladrey/en_US.html"><img src="http://philabar.org/WebObjects/PBA.woa/Contents/WebServerResources/CMSResources/McGladrey_web.jpg" width="190" height="43" alt="McGladrey"></a>	 <hr> <a href="http://www.ballardspahr.com"><img src="http://philabar.org/WebObjects/PBA.woa/Contents/WebServerResources/CMSResources/Ballard-sponsor-logo_yellow.jpg" width="190" height="38" alt="Ballard Spahr LLP" border="0"></a> <hr> <a href="http://www.dilworthlaw.com/"><img src="http://philabar.org/WebObjects/PBA.woa/Contents/WebServerResources/CMSResources/DilworthPaxsonLogo_web.jpg" width="190" height="59" alt="Dilworth Paxson"></a>	 <hr> <a href="http://www.eckertseamans.com"><img src="http://philabar.org/WebObjects/PBA.woa/Contents/WebServerResources/CMSResources/eckertseamans_logo_web.jpg" width="190" height="77" alt="Eckert Seamans Attorneys at Law" border="0"></a> <hr> <a href="http://www.finemanlawfirm.com/"><img src="http://philabar.org/WebObjects/PBA.woa/Contents/WebServerResources/CMSResources/fineman_logo_web.jpg" width="190" height="34" alt="Fineman Krekstein &amp; Harris"></a>		 <hr> <a href="http://www.duanemorris.com"><img src="http://philabar.org/WebObjects/PBA.woa/Contents/WebServerResources/CMSResources/DMlogo_web.jpg" width="190" height="28.5" alt="Eckert Seamans Attorneys at Law" border="0"></a> <hr> <a href="https://www.wellsfargo.com/"><img src="http://philabar.org/WebObjects/PBA.woa/Contents/WebServerResources/CMSResources/wellsfargo_logo_web.jpg" width="91" height="91" alt="Wells Fargo" border="0"></a> <hr>
</div>
</div>

<div class="solid green centered block">
<div class="block-inner">
<h2 class="small title">Join the ListServ</h2>
<a href="#" class="pba-small grey button">Click Herev</a>
</div>
</div>
							
<div class="subnav orange titled outline block">
<div class="block-inner">
<h2 class="small title">Committees</h2>
<div class="content">
	<ul id="subnav" class="large stackednav navigation nobullet strong link list">

	
		<li id="050713141943909">
			
				<a title="Antitrust Law" href="http://philabar.org/page/BLSAntitrust?appNum=2">Antitrust Law</a>
			
			
			
		</li>
	
		<li id="050713141957746">
			
				<a title="Banking &amp; Commercial Finance" href="http://philabar.org/page/BLSBanking?appNum=2">Banking &amp; Commercial Finance</a>
			
			
			
		</li>
	
		<li id="050713143230763">
			
				<a title="Bankruptcy Law" href="http://philabar.org/page/BLSBankruptcy?appNum=2">Bankruptcy Law</a>
			
			
			
		</li>
	
		<li id="050713143241549">
			
				<a title="Business Litigation" href="http://philabar.org/page/BLSLitigation?appNum=2">Business Litigation</a>
			
			
			
		</li>
	
		<li id="050713143257362">
			
				<a title="Cyberlaw" href="http://philabar.org/page/BLSCyberspace?appNum=2">Cyberlaw</a>
			
			
			
		</li>
	
		<li id="050713143312010">
			
				<a title="Franchise Law" href="http://philabar.org/page/BLSFranchise?appNum=2">Franchise Law</a>
			
			
			
		</li>
	
		<li id="050713143320360">
			
				<a title="Health Care Law" href="http://philabar.org/page/BLSHealthCare?appNum=2">Health Care Law</a>
			
			
			
		</li>
	
		<li id="050713143351966">
			
				<a title="Insurance Law" href="http://philabar.org/page/BLSInsurance?appNum=2">Insurance Law</a>
			
			
			
		</li>
	
		<li id="050713143412002">
			
				<a title="Investment Companies" href="http://philabar.org/page/BLSInvestment?appNum=2">Investment Companies</a>
			
			
			
		</li>
	
		<li id="050713143423952">
			
				<a title="Mergers &amp; Acquisitions" href="http://philabar.org/page/BLSMergers?appNum=2">Mergers &amp; Acquisitions</a>
			
			
			
		</li>
	
		<li id="050713143436801">
			
				<a title="Securities Regulation" href="http://philabar.org/page/BLSSecurities?appNum=2">Securities Regulation</a>
			
			
			
		</li>
	
		<li id="050713143448219">
			
				<a title="Small Business" href="http://philabar.org/page/BLSSmallBus?appNum=2">Small Business</a>
			
			
			
		</li>
	
		<li id="050713143510318">
			
				<a title="Venture Capital &amp; Private Equity Law" href="http://philabar.org/page/BLSVC?appNum=2">Venture Capital &amp; Private Equity Law</a>
			
			
			
		</li>
	
</ul>
</div>
			
</div>
</div>			
							
							
							
							
							
							
							

			<?php // include("sidebar-right.php"); ?>
		</div><!-- /second-sidebar -->
	</div>

<?php cb(); ?>